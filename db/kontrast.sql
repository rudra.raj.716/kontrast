-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2021 at 12:17 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kontrast`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `aboutus_id` int(11) NOT NULL,
  `aboutus_title` varchar(255) NOT NULL,
  `aboutus_description` text NOT NULL,
  `aboutus_image` varchar(255) NOT NULL,
  `aboutus_image_alt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`aboutus_id`, `aboutus_title`, `aboutus_description`, `aboutus_image`, `aboutus_image_alt`) VALUES
(2, 'about us 1', 'We plan and analyze our movement not by just crafting creative inputs but help you build a brand strategy that assess whether your product delivers the value that your consumers desire. We combine hard metrics and creative magic to build a consumer focused brand strategy that goes far beyond regularly practiced brand communications.', 'ac5f6-dell.jpg', 'image');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL,
  `client_title` varchar(250) NOT NULL,
  `client_image` varchar(250) NOT NULL,
  `client_image_alt` varchar(255) NOT NULL,
  `client_link` varchar(255) NOT NULL,
  `rankorder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `client_title`, `client_image`, `client_image_alt`, `client_link`, `rankorder`) VALUES
(18, 'Renault', 'd68d9-renault.jpg', 'a', '', 1),
(19, 'Oppo', 'a79d0-oppo.jpg', 'b', '', 2),
(20, 'Sharetech', '9b920-sharetech.png', 'c', '', 3),
(21, 'trundl', '9b09f-trundl.png', 'd', '', 4),
(22, '360 fitness', '42922-360fitnesssuperstore.jpg', 'e', '', 5),
(23, 'Vitality bowls', '98dce-vitalitybowls.png', 'f', '', 6),
(24, 'Book trip', '23165-bookotrip.jpg', 'g', '', 7),
(25, 'Speackinc', '84314-speackinc.png', 'h', '', 8),
(26, 'FIC Global', '69dd1-ficglobal.png', 'i', '', 9),
(27, 'Fineacre', '12214-fineacre.png', 'j', '', 10),
(28, 'Argo', 'c4641-argo.png', 'k', '', 11),
(29, 'Narika', 'b9641-narika.png', 'l', '', 12);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_subject` varchar(255) NOT NULL,
  `contact_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `contact_name`, `contact_email`, `contact_subject`, `contact_message`) VALUES
(14, 'sushant chapagain', 'thirstyhacker97@gmail.com', '', 'sdasjdkakd'),
(15, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test'),
(16, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'tesst'),
(17, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test'),
(18, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test'),
(19, 'Sagun Siwakoti', 'sagunsiwakoti25@gmail.com', '', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customerNumber` int(11) NOT NULL,
  `customerName` varchar(50) NOT NULL,
  `contactLastName` varchar(50) NOT NULL,
  `contactFirstName` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `addressLine1` varchar(50) NOT NULL,
  `addressLine2` varchar(50) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) DEFAULT NULL,
  `postalCode` varchar(15) DEFAULT NULL,
  `country` varchar(50) NOT NULL,
  `salesRepEmployeeNumber` int(11) DEFAULT NULL,
  `creditLimit` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customerNumber`, `customerName`, `contactLastName`, `contactFirstName`, `phone`, `addressLine1`, `addressLine2`, `city`, `state`, `postalCode`, `country`, `salesRepEmployeeNumber`, `creditLimit`) VALUES
(103, 'Atelier graphique', 'Schmitt', 'Carine ', '40.32.2555', '54, rue Royale', NULL, 'Nantes', NULL, '44000', 'France', 1370, 21000),
(112, 'Signal Gift Stores', 'King', 'Jean', '7025551838', '8489 Strong St.', NULL, 'Las Vegas', 'NV', '83030', 'USA', 1166, 71800),
(114, 'Australian Collectors, Co.', 'Ferguson', 'Peter', '03 9520 4555', '636 St Kilda Road', 'Level 3', 'Melbourne', 'Victoria', '3004', 'Australia', 1611, 117300),
(119, 'La Rochelle Gifts', 'Labrune', 'Janine ', '40.67.8555', '67, rue des Cinquante Otages', NULL, 'Nantes', NULL, '44000', 'France', 1370, 118200),
(121, 'Baane Mini Imports', 'Bergulfsen', 'Jonas ', '07-98 9555', 'Erling Skakkes gate 78', NULL, 'Stavern', NULL, '4110', 'Norway', 1504, 81700),
(124, 'Mini Gifts Distributors Ltd.', 'Nelson', 'Susan', '4155551450', '5677 Strong St.', NULL, 'San Rafael', 'CA', '97562', 'USA', 1165, 210500),
(125, 'Havel & Zbyszek Co', 'Piestrzeniewicz', 'Zbyszek ', '(26) 642-7555', 'ul. Filtrowa 68', NULL, 'Warszawa', NULL, '01-012', 'Poland', NULL, 0),
(128, 'Blauer See Auto, Co.', 'Keitel', 'Roland', '+49 69 66 90 2555', 'Lyonerstr. 34', NULL, 'Frankfurt', NULL, '60528', 'Germany', 1504, 59700),
(129, 'Mini Wheels Co.', 'Murphy', 'Julie', '6505555787', '5557 North Pendale Street', NULL, 'San Francisco', 'CA', '94217', 'USA', 1165, 64600),
(131, 'Land of Toys Inc.', 'Lee', 'Kwai', '2125557818', '897 Long Airport Avenue', NULL, 'NYC', 'NY', '10022', 'USA', 1323, 114900),
(141, 'Euro+ Shopping Channel', 'Freyre', 'Diego ', '(91) 555 94 44', 'C/ Moralzarzal, 86', NULL, 'Madrid', NULL, '28034', 'Spain', 1370, 227600),
(144, 'Volvo Model Replicas, Co', 'Berglund', 'Christina ', '0921-12 3555', 'Berguvsvägen  8', NULL, 'Luleå', NULL, 'S-958 22', 'Sweden', 1504, 53100),
(145, 'Danish Wholesale Imports', 'Petersen', 'Jytte ', '31 12 3555', 'Vinbæltet 34', NULL, 'Kobenhavn', NULL, '1734', 'Denmark', 1401, 83400),
(146, 'Saveley & Henriot, Co.', 'Saveley', 'Mary ', '78.32.5555', '2, rue du Commerce', NULL, 'Lyon', NULL, '69004', 'France', 1337, 123900),
(148, 'Dragon Souveniers, Ltd.', 'Natividad', 'Eric', '+65 221 7555', 'Bronz Sok.', 'Bronz Apt. 3/6 Tesvikiye', 'Singapore', NULL, '079903', 'Singapore', 1621, 103800),
(151, 'Muscle Machine Inc', 'Young', 'Jeff', '2125557413', '4092 Furth Circle', 'Suite 400', 'NYC', 'NY', '10022', 'USA', 1286, 138500),
(157, 'Diecast Classics Inc.', 'Leong', 'Kelvin', '2155551555', '7586 Pompton St.', NULL, 'Allentown', 'PA', '70267', 'USA', 1216, 100600),
(161, 'Technics Stores Inc.', 'Hashimoto', 'Juri', '6505556809', '9408 Furth Circle', NULL, 'Burlingame', 'CA', '94217', 'USA', 1165, 84600),
(166, 'Handji Gifts& Co', 'Victorino', 'Wendy', '+65 224 1555', '106 Linden Road Sandown', '2nd Floor', 'Singapore', NULL, '069045', 'Singapore', 1612, 97900),
(167, 'Herkku Gifts', 'Oeztan', 'Veysel', '+47 2267 3215', 'Brehmen St. 121', 'PR 334 Sentrum', 'Bergen', NULL, 'N 5804', 'Norway  ', 1504, 96800),
(168, 'American Souvenirs Inc', 'Franco', 'Keith', '2035557845', '149 Spinnaker Dr.', 'Suite 101', 'New Haven', 'CT', '97823', 'USA', 1286, 0),
(169, 'Porto Imports Co.', 'de Castro', 'Isabel ', '(1) 356-5555', 'Estrada da saúde n. 58', NULL, 'Lisboa', NULL, '1756', 'Portugal', NULL, 0),
(171, 'Daedalus Designs Imports', 'Rancé', 'Martine ', '20.16.1555', '184, chaussée de Tournai', NULL, 'Lille', NULL, '59000', 'France', 1370, 82900),
(172, 'La Corne D\'abondance, Co.', 'Bertrand', 'Marie', '(1) 42.34.2555', '265, boulevard Charonne', NULL, 'Paris', NULL, '75012', 'France', 1337, 84300),
(173, 'Cambridge Collectables Co.', 'Tseng', 'Jerry', '6175555555', '4658 Baden Av.', NULL, 'Cambridge', 'MA', '51247', 'USA', 1188, 43400),
(175, 'Gift Depot Inc.', 'King', 'Julie', '2035552570', '25593 South Bay Ln.', NULL, 'Bridgewater', 'CT', '97562', 'USA', 1323, 84300),
(177, 'Osaka Souveniers Co.', 'Kentary', 'Mory', '+81 06 6342 5555', '1-6-20 Dojima', NULL, 'Kita-ku', 'Osaka', ' 530-0003', 'Japan', 1621, 81200),
(181, 'Vitachrome Inc.', 'Frick', 'Michael', '2125551500', '2678 Kingston Rd.', 'Suite 101', 'NYC', 'NY', '10022', 'USA', 1286, 76400),
(186, 'Toys of Finland, Co.', 'Karttunen', 'Matti', '90-224 8555', 'Keskuskatu 45', NULL, 'Helsinki', NULL, '21240', 'Finland', 1501, 96500),
(187, 'AV Stores, Co.', 'Ashworth', 'Rachel', '(171) 555-1555', 'Fauntleroy Circus', NULL, 'Manchester', NULL, 'EC2 5NT', 'UK', 1501, 136800),
(189, 'Clover Collections, Co.', 'Cassidy', 'Dean', '+353 1862 1555', '25 Maiden Lane', 'Floor No. 4', 'Dublin', NULL, '2', 'Ireland', 1504, 69400),
(198, 'Auto-Moto Classics Inc.', 'Taylor', 'Leslie', '6175558428', '16780 Pompton St.', NULL, 'Brickhaven', 'MA', '58339', 'USA', 1216, 23000),
(201, 'UK Collectables, Ltd.', 'Devon', 'Elizabeth', '(171) 555-2282', '12, Berkeley Gardens Blvd', NULL, 'Liverpool', NULL, 'WX1 6LT', 'UK', 1501, 92700),
(202, 'Canadian Gift Exchange Network', 'Tamuri', 'Yoshi ', '(604) 555-3392', '1900 Oak St.', NULL, 'Vancouver', 'BC', 'V3F 2K1', 'Canada', 1323, 90300),
(204, 'Online Mini Collectables', 'Barajas', 'Miguel', '6175557555', '7635 Spinnaker Dr.', NULL, 'Brickhaven', 'MA', '58339', 'USA', 1188, 68700),
(205, 'Toys4GrownUps.com', 'Young', 'Julie', '6265557265', '78934 Hillside Dr.', NULL, 'Pasadena', 'CA', '90003', 'USA', 1166, 90700),
(206, 'Asian Shopping Network, Co', 'Walker', 'Brydey', '+612 9411 1555', 'Suntec Tower Three', '8 Temasek', 'Singapore', NULL, '038988', 'Singapore', NULL, 0),
(209, 'Mini Caravy', 'Citeaux', 'Frédérique ', '88.60.1555', '24, place Kléber', NULL, 'Strasbourg', NULL, '67000', 'France', 1370, 53800),
(211, 'King Kong Collectables, Co.', 'Gao', 'Mike', '+852 2251 1555', 'Bank of China Tower', '1 Garden Road', 'Central Hong Kong', NULL, NULL, 'Hong Kong', 1621, 58600),
(216, 'Enaco Distributors', 'Saavedra', 'Eduardo ', '(93) 203 4555', 'Rambla de Cataluña, 23', NULL, 'Barcelona', NULL, '08022', 'Spain', 1702, 60300),
(219, 'Boards & Toys Co.', 'Young', 'Mary', '3105552373', '4097 Douglas Av.', NULL, 'Glendale', 'CA', '92561', 'USA', 1166, 11000),
(223, 'Natürlich Autos', 'Kloss', 'Horst ', '0372-555188', 'Taucherstraße 10', NULL, 'Cunewalde', NULL, '01307', 'Germany', NULL, 0),
(227, 'Heintze Collectables', 'Ibsen', 'Palle', '86 21 3555', 'Smagsloget 45', NULL, 'Århus', NULL, '8200', 'Denmark', 1401, 120800),
(233, 'Québec Home Shopping Network', 'Fresnière', 'Jean ', '(514) 555-8054', '43 rue St. Laurent', NULL, 'Montréal', 'Québec', 'H1J 1C3', 'Canada', 1286, 48700),
(237, 'ANG Resellers', 'Camino', 'Alejandra ', '(91) 745 6555', 'Gran Vía, 1', NULL, 'Madrid', NULL, '28001', 'Spain', NULL, 0),
(239, 'Collectable Mini Designs Co.', 'Thompson', 'Valarie', '7605558146', '361 Furth Circle', NULL, 'San Diego', 'CA', '91217', 'USA', 1166, 105000),
(240, 'giftsbymail.co.uk', 'Bennett', 'Helen ', '(198) 555-8888', 'Garden House', 'Crowther Way 23', 'Cowes', 'Isle of Wight', 'PO31 7PJ', 'UK', 1501, 93900),
(242, 'Alpha Cognac', 'Roulet', 'Annette ', '61.77.6555', '1 rue Alsace-Lorraine', NULL, 'Toulouse', NULL, '31000', 'France', 1370, 61100),
(247, 'Messner Shopping Network', 'Messner', 'Renate ', '069-0555984', 'Magazinweg 7', NULL, 'Frankfurt', NULL, '60528', 'Germany', NULL, 0),
(249, 'Amica Models & Co.', 'Accorti', 'Paolo ', '011-4988555', 'Via Monte Bianco 34', NULL, 'Torino', NULL, '10100', 'Italy', 1401, 113000),
(250, 'Lyon Souveniers', 'Da Silva', 'Daniel', '+33 1 46 62 7555', '27 rue du Colonel Pierre Avia', NULL, 'Paris', NULL, '75508', 'France', 1337, 68100),
(256, 'Auto Associés & Cie.', 'Tonini', 'Daniel ', '30.59.8555', '67, avenue de l\'Europe', NULL, 'Versailles', NULL, '78000', 'France', 1370, 77900),
(259, 'Toms Spezialitäten, Ltd', 'Pfalzheim', 'Henriette ', '0221-5554327', 'Mehrheimerstr. 369', NULL, 'Köln', NULL, '50739', 'Germany', 1504, 120400),
(260, 'Royal Canadian Collectables, Ltd.', 'Lincoln', 'Elizabeth ', '(604) 555-4555', '23 Tsawassen Blvd.', NULL, 'Tsawassen', 'BC', 'T2F 8M4', 'Canada', 1323, 89600),
(273, 'Franken Gifts, Co', 'Franken', 'Peter ', '089-0877555', 'Berliner Platz 43', NULL, 'München', NULL, '80805', 'Germany', NULL, 0),
(276, 'Anna\'s Decorations, Ltd', 'O\'Hara', 'Anna', '02 9936 8555', '201 Miller Street', 'Level 15', 'North Sydney', 'NSW', '2060', 'Australia', 1611, 107800),
(278, 'Rovelli Gifts', 'Rovelli', 'Giovanni ', '035-640555', 'Via Ludovico il Moro 22', NULL, 'Bergamo', NULL, '24100', 'Italy', 1401, 119600),
(282, 'Souveniers And Things Co.', 'Huxley', 'Adrian', '+61 2 9495 8555', 'Monitor Money Building', '815 Pacific Hwy', 'Chatswood', 'NSW', '2067', 'Australia', 1611, 93300),
(286, 'Marta\'s Replicas Co.', 'Hernandez', 'Marta', '6175558555', '39323 Spinnaker Dr.', NULL, 'Cambridge', 'MA', '51247', 'USA', 1216, 123700),
(293, 'BG&E Collectables', 'Harrison', 'Ed', '+41 26 425 50 01', 'Rte des Arsenaux 41 ', NULL, 'Fribourg', NULL, '1700', 'Switzerland', NULL, 0),
(298, 'Vida Sport, Ltd', 'Holz', 'Mihael', '0897-034555', 'Grenzacherweg 237', NULL, 'Genève', NULL, '1203', 'Switzerland', 1702, 141300),
(299, 'Norway Gifts By Mail, Co.', 'Klaeboe', 'Jan', '+47 2212 1555', 'Drammensveien 126A', 'PB 211 Sentrum', 'Oslo', NULL, 'N 0106', 'Norway  ', 1504, 95100),
(303, 'Schuyler Imports', 'Schuyler', 'Bradley', '+31 20 491 9555', 'Kingsfordweg 151', NULL, 'Amsterdam', NULL, '1043 GR', 'Netherlands', NULL, 0),
(307, 'Der Hund Imports', 'Andersen', 'Mel', '030-0074555', 'Obere Str. 57', NULL, 'Berlin', NULL, '12209', 'Germany', NULL, 0),
(311, 'Oulu Toy Supplies, Inc.', 'Koskitalo', 'Pirkko', '981-443655', 'Torikatu 38', NULL, 'Oulu', NULL, '90110', 'Finland', 1501, 90500),
(314, 'Petit Auto', 'Dewey', 'Catherine ', '(02) 5554 67', 'Rue Joseph-Bens 532', NULL, 'Bruxelles', NULL, 'B-1180', 'Belgium', 1401, 79900),
(319, 'Mini Classics', 'Frick', 'Steve', '9145554562', '3758 North Pendale Street', NULL, 'White Plains', 'NY', '24067', 'USA', 1323, 102700),
(320, 'Mini Creations Ltd.', 'Huang', 'Wing', '5085559555', '4575 Hillside Dr.', NULL, 'New Bedford', 'MA', '50553', 'USA', 1188, 94500),
(321, 'Corporate Gift Ideas Co.', 'Brown', 'Julie', '6505551386', '7734 Strong St.', NULL, 'San Francisco', 'CA', '94217', 'USA', 1165, 105000),
(323, 'Down Under Souveniers, Inc', 'Graham', 'Mike', '+64 9 312 5555', '162-164 Grafton Road', 'Level 2', 'Auckland  ', NULL, NULL, 'New Zealand', 1612, 88000),
(324, 'Stylish Desk Decors, Co.', 'Brown', 'Ann ', '(171) 555-0297', '35 King George', NULL, 'London', NULL, 'WX3 6FW', 'UK', 1501, 77000),
(328, 'Tekni Collectables Inc.', 'Brown', 'William', '2015559350', '7476 Moss Rd.', NULL, 'Newark', 'NJ', '94019', 'USA', 1323, 43000),
(333, 'Australian Gift Network, Co', 'Calaghan', 'Ben', '61-7-3844-6555', '31 Duncan St. West End', NULL, 'South Brisbane', 'Queensland', '4101', 'Australia', 1611, 51600),
(334, 'Suominen Souveniers', 'Suominen', 'Kalle', '+358 9 8045 555', 'Software Engineering Center', 'SEC Oy', 'Espoo', NULL, 'FIN-02271', 'Finland', 1501, 98800),
(335, 'Cramer Spezialitäten, Ltd', 'Cramer', 'Philip ', '0555-09555', 'Maubelstr. 90', NULL, 'Brandenburg', NULL, '14776', 'Germany', NULL, 0),
(339, 'Classic Gift Ideas, Inc', 'Cervantes', 'Francisca', '2155554695', '782 First Street', NULL, 'Philadelphia', 'PA', '71270', 'USA', 1188, 81100),
(344, 'CAF Imports', 'Fernandez', 'Jesus', '+34 913 728 555', 'Merchants House', '27-30 Merchant\'s Quay', 'Madrid', NULL, '28023', 'Spain', 1702, 59600),
(347, 'Men \'R\' US Retailers, Ltd.', 'Chandler', 'Brian', '2155554369', '6047 Douglas Av.', NULL, 'Los Angeles', 'CA', '91003', 'USA', 1166, 57700),
(348, 'Asian Treasures, Inc.', 'McKenna', 'Patricia ', '2967 555', '8 Johnstown Road', NULL, 'Cork', 'Co. Cork', NULL, 'Ireland', NULL, 0),
(350, 'Marseille Mini Autos', 'Lebihan', 'Laurence ', '91.24.4555', '12, rue des Bouchers', NULL, 'Marseille', NULL, '13008', 'France', 1337, 65000),
(353, 'Reims Collectables', 'Henriot', 'Paul ', '26.47.1555', '59 rue de l\'Abbaye', NULL, 'Reims', NULL, '51100', 'France', 1337, 81100),
(356, 'SAR Distributors, Co', 'Kuger', 'Armand', '+27 21 550 3555', '1250 Pretorius Street', NULL, 'Hatfield', 'Pretoria', '0028', 'South Africa', NULL, 0),
(357, 'GiftsForHim.com', 'MacKinlay', 'Wales', '64-9-3763555', '199 Great North Road', NULL, 'Auckland', NULL, NULL, 'New Zealand', 1612, 77700),
(361, 'Kommission Auto', 'Josephs', 'Karin', '0251-555259', 'Luisenstr. 48', NULL, 'Münster', NULL, '44087', 'Germany', NULL, 0),
(362, 'Gifts4AllAges.com', 'Yoshido', 'Juri', '6175559555', '8616 Spinnaker Dr.', NULL, 'Boston', 'MA', '51003', 'USA', 1216, 41900),
(363, 'Online Diecast Creations Co.', 'Young', 'Dorothy', '6035558647', '2304 Long Airport Avenue', NULL, 'Nashua', 'NH', '62005', 'USA', 1216, 114200),
(369, 'Lisboa Souveniers, Inc', 'Rodriguez', 'Lino ', '(1) 354-2555', 'Jardim das rosas n. 32', NULL, 'Lisboa', NULL, '1675', 'Portugal', NULL, 0),
(376, 'Precious Collectables', 'Urs', 'Braun', '0452-076555', 'Hauptstr. 29', NULL, 'Bern', NULL, '3012', 'Switzerland', 1702, 0),
(379, 'Collectables For Less Inc.', 'Nelson', 'Allen', '6175558555', '7825 Douglas Av.', NULL, 'Brickhaven', 'MA', '58339', 'USA', 1188, 70700),
(381, 'Royale Belge', 'Cartrain', 'Pascale ', '(071) 23 67 2555', 'Boulevard Tirou, 255', NULL, 'Charleroi', NULL, 'B-6000', 'Belgium', 1401, 23500),
(382, 'Salzburg Collectables', 'Pipps', 'Georg ', '6562-9555', 'Geislweg 14', NULL, 'Salzburg', NULL, '5020', 'Austria', 1401, 71700),
(385, 'Cruz & Sons Co.', 'Cruz', 'Arnold', '+63 2 555 3587', '15 McCallum Street', 'NatWest Center #13-03', 'Makati City', NULL, '1227 MM', 'Philippines', 1621, 81500),
(386, 'L\'ordine Souveniers', 'Moroni', 'Maurizio ', '0522-556555', 'Strada Provinciale 124', NULL, 'Reggio Emilia', NULL, '42100', 'Italy', 1401, 121400),
(398, 'Tokyo Collectables, Ltd', 'Shimamura', 'Akiko', '+81 3 3584 0555', '2-2-8 Roppongi', NULL, 'Minato-ku', 'Tokyo', '106-0032', 'Japan', 1621, 94400),
(406, 'Auto Canal+ Petit', 'Perrier', 'Dominique', '(1) 47.55.6555', '25, rue Lauriston', NULL, 'Paris', NULL, '75016', 'France', 1337, 95000),
(409, 'Stuttgart Collectable Exchange', 'Müller', 'Rita ', '0711-555361', 'Adenauerallee 900', NULL, 'Stuttgart', NULL, '70563', 'Germany', NULL, 0),
(412, 'Extreme Desk Decorations, Ltd', 'McRoy', 'Sarah', '04 499 9555', '101 Lambton Quay', 'Level 11', 'Wellington', NULL, NULL, 'New Zealand', 1612, 86800),
(415, 'Bavarian Collectables Imports, Co.', 'Donnermeyer', 'Michael', ' +49 89 61 08 9555', 'Hansastr. 15', NULL, 'Munich', NULL, '80686', 'Germany', 1504, 77000),
(424, 'Classic Legends Inc.', 'Hernandez', 'Maria', '2125558493', '5905 Pompton St.', 'Suite 750', 'NYC', 'NY', '10022', 'USA', 1286, 67500),
(443, 'Feuer Online Stores, Inc', 'Feuer', 'Alexander ', '0342-555176', 'Heerstr. 22', NULL, 'Leipzig', NULL, '04179', 'Germany', NULL, 0),
(447, 'Gift Ideas Corp.', 'Lewis', 'Dan', '2035554407', '2440 Pompton St.', NULL, 'Glendale', 'CT', '97561', 'USA', 1323, 49700),
(448, 'Scandinavian Gift Ideas', 'Larsson', 'Martha', '0695-34 6555', 'Åkergatan 24', NULL, 'Bräcke', NULL, 'S-844 67', 'Sweden', 1504, 116400),
(450, 'The Sharp Gifts Warehouse', 'Frick', 'Sue', '4085553659', '3086 Ingle Ln.', NULL, 'San Jose', 'CA', '94217', 'USA', 1165, 77600),
(452, 'Mini Auto Werke', 'Mendel', 'Roland ', '7675-3555', 'Kirchgasse 6', NULL, 'Graz', NULL, '8010', 'Austria', 1401, 45300),
(455, 'Super Scale Inc.', 'Murphy', 'Leslie', '2035559545', '567 North Pendale Street', NULL, 'New Haven', 'CT', '97823', 'USA', 1286, 95400),
(456, 'Microscale Inc.', 'Choi', 'Yu', '2125551957', '5290 North Pendale Street', 'Suite 200', 'NYC', 'NY', '10022', 'USA', 1286, 39800),
(458, 'Corrida Auto Replicas, Ltd', 'Sommer', 'Martín ', '(91) 555 22 82', 'C/ Araquil, 67', NULL, 'Madrid', NULL, '28023', 'Spain', 1702, 104600),
(459, 'Warburg Exchange', 'Ottlieb', 'Sven ', '0241-039123', 'Walserweg 21', NULL, 'Aachen', NULL, '52066', 'Germany', NULL, 0),
(462, 'FunGiftIdeas.com', 'Benitez', 'Violeta', '5085552555', '1785 First Street', NULL, 'New Bedford', 'MA', '50553', 'USA', 1216, 85800),
(465, 'Anton Designs, Ltd.', 'Anton', 'Carmen', '+34 913 728555', 'c/ Gobelas, 19-1 Urb. La Florida', NULL, 'Madrid', NULL, '28023', 'Spain', NULL, 0),
(471, 'Australian Collectables, Ltd', 'Clenahan', 'Sean', '61-9-3844-6555', '7 Allen Street', NULL, 'Glen Waverly', 'Victoria', '3150', 'Australia', 1611, 60300),
(473, 'Frau da Collezione', 'Ricotti', 'Franco', '+39 022515555', '20093 Cologno Monzese', 'Alessandro Volta 16', 'Milan', NULL, NULL, 'Italy', 1401, 34800),
(475, 'West Coast Collectables Co.', 'Thompson', 'Steve', '3105553722', '3675 Furth Circle', NULL, 'Burbank', 'CA', '94019', 'USA', 1166, 55400),
(477, 'Mit Vergnügen & Co.', 'Moos', 'Hanna ', '0621-08555', 'Forsterstr. 57', NULL, 'Mannheim', NULL, '68306', 'Germany', NULL, 0),
(480, 'Kremlin Collectables, Co.', 'Semenov', 'Alexander ', '+7 812 293 0521', '2 Pobedy Square', NULL, 'Saint Petersburg', NULL, '196143', 'Russia', NULL, 0),
(481, 'Raanan Stores, Inc', 'Altagar,G M', 'Raanan', '+ 972 9 959 8555', '3 Hagalim Blv.', NULL, 'Herzlia', NULL, '47625', 'Israel', NULL, 0),
(484, 'Iberia Gift Imports, Corp.', 'Roel', 'José Pedro ', '(95) 555 82 82', 'C/ Romero, 33', NULL, 'Sevilla', NULL, '41101', 'Spain', 1702, 65700),
(486, 'Motor Mint Distributors Inc.', 'Salazar', 'Rosa', '2155559857', '11328 Douglas Av.', NULL, 'Philadelphia', 'PA', '71270', 'USA', 1323, 72600),
(487, 'Signal Collectibles Ltd.', 'Taylor', 'Sue', '4155554312', '2793 Furth Circle', NULL, 'Brisbane', 'CA', '94217', 'USA', 1165, 60300),
(489, 'Double Decker Gift Stores, Ltd', 'Smith', 'Thomas ', '(171) 555-7555', '120 Hanover Sq.', NULL, 'London', NULL, 'WA1 1DP', 'UK', 1501, 43300),
(495, 'Diecast Collectables', 'Franco', 'Valarie', '6175552555', '6251 Ingle Ln.', NULL, 'Boston', 'MA', '51003', 'USA', 1188, 85100),
(496, 'Kelly\'s Gift Shop', 'Snowden', 'Tony', '+64 9 5555500', 'Arenales 1938 3\'A\'', NULL, 'Auckland  ', NULL, NULL, 'New Zealand', 1612, 110000);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `employeeNumber` int(11) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `officeCode` varchar(10) NOT NULL,
  `file_url` varchar(250) CHARACTER SET utf8 NOT NULL,
  `jobTitle` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employeeNumber`, `lastName`, `firstName`, `extension`, `email`, `officeCode`, `file_url`, `jobTitle`) VALUES
(1002, 'Murphy', 'Diane', 'x5800', 'dmurphy@classicmodelcars.com', '1', '', 'President'),
(1056, 'Patterson', 'Mary', 'x4611', 'mpatterso@classicmodelcars.com', '1', '', 'VP Sales'),
(1076, 'Firrelli', 'Jeff', 'x9273', 'jfirrelli@classicmodelcars.com', '1', '', 'VP Marketing'),
(1088, 'Patterson', 'William', 'x4871', 'wpatterson@classicmodelcars.com', '6', '', 'Sales Manager (APAC)'),
(1102, 'Bondur', 'Gerard', 'x5408', 'gbondur@classicmodelcars.com', '4', 'pdftest.pdf', 'Sale Manager (EMEA)'),
(1143, 'Bow', 'Anthony', 'x5428', 'abow@classicmodelcars.com', '1', '', 'Sales Manager (NA)'),
(1165, 'Jennings', 'Leslie', 'x3291', 'ljennings@classicmodelcars.com', '1', '', 'Sales Rep'),
(1166, 'Thompson', 'Leslie', 'x4065', 'lthompson@classicmodelcars.com', '1', '', 'Sales Rep'),
(1188, 'Firrelli', 'Julie', 'x2173', 'jfirrelli@classicmodelcars.com', '2', 'test-2.pdf', 'Sales Rep'),
(1216, 'Patterson', 'Steve', 'x4334', 'spatterson@classicmodelcars.com', '2', '', 'Sales Rep'),
(1286, 'Tseng', 'Foon Yue', 'x2248', 'ftseng@classicmodelcars.com', '3', '', 'Sales Rep'),
(1323, 'Vanauf', 'George', 'x4102', 'gvanauf@classicmodelcars.com', '3', '', 'Sales Rep'),
(1337, 'Bondur', 'Loui', 'x6493', 'lbondur@classicmodelcars.com', '4', '', 'Sales Rep'),
(1370, 'Hernandez', 'Gerard', 'x2028', 'ghernande@classicmodelcars.com', '4', '', 'Sales Rep'),
(1401, 'Castillo', 'Pamela', 'x2759', 'pcastillo@classicmodelcars.com', '4', '', 'Sales Rep'),
(1501, 'Bott', 'Larry', 'x2311', 'lbott@classicmodelcars.com', '7', '', 'Sales Rep'),
(1504, 'Jones', 'Barry', 'x102', 'bjones@classicmodelcars.com', '7', '', 'Sales Rep'),
(1611, 'Fixter', 'Andy', 'x101', 'afixter@classicmodelcars.com', '6', '', 'Sales Rep'),
(1612, 'Marsh', 'Peter', 'x102', 'pmarsh@classicmodelcars.com', '6', '', 'Sales Rep'),
(1619, 'King', 'Tom', 'x103', 'tking@classicmodelcars.com', '6', '', 'Sales Rep'),
(1621, 'Nishi', 'Mami', 'x101', 'mnishi@classicmodelcars.com', '5', '', 'Sales Rep'),
(1625, 'Kato', 'Yoshimi', 'x102', 'ykato@classicmodelcars.com', '5', '', 'Sales Rep'),
(1702, 'Gerard', 'Martin', 'x2312', 'mgerard@classicmodelcars.com', '4', '', 'Sales Rep');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `faq_id` int(11) NOT NULL,
  `faq_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faq_description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `faq_title`, `faq_description`) VALUES
(1, 'What should  i do if my interior is broken?', 'Lorem ipsum dolor sit amet, Cnsectetur adipisicing elit. Eos quos incidunt, perspiciatis, ad saepe, magnam error adipisci vitae ut provident alias! Odio debitis error ipsum molestiae voluptas accusantium quibusdam animi, soluta explicabo asperiores aliquid, modi natus suscipit deleniti. Corrupti, autem.\r\n\r\n'),
(2, 'What is your location?', 'Lorem ipsum dolor sit amet, Cnsectetur adipisicing elit. Eos quos incidunt, perspiciatis, ad saepe, magnam error adipisci vitae ut provident alias! Odio debitis error ipsum molestiae voluptas accusantium quibusdam animi, soluta explicabo asperiores aliquid, modi natus suscipit deleniti. Corrupti, autem.\r\n\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `folder_id` int(11) NOT NULL,
  `folder_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`folder_id`, `folder_name`, `project_id`) VALUES
(4, 'Page Photographs', 0),
(6, 'Notices', 0);

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `food_id` int(11) NOT NULL,
  `food_title` varchar(255) DEFAULT NULL,
  `food_description` varchar(255) DEFAULT NULL,
  `food_price` int(11) DEFAULT NULL,
  `food_image` varchar(255) DEFAULT NULL,
  `food_category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`food_id`, `food_title`, `food_description`, `food_price`, `food_image`, `food_category_id`) VALUES
(1, 'Chaumin', 'just a chaumin', 100, '42048-maxresdefault.jpg', 1),
(2, 'Momo', 'just a momo', 120, '419a3-momo.jpg', 1),
(3, 'Biryani', 'just a biryani', 250, 'a4548-gettyimages-639704020-612x612.jpg', 2),
(4, 'Daal Bhat', 'just daal bhat', 250, 'c5ca8-download.jpg', 4);

-- --------------------------------------------------------

--
-- Table structure for table `food_category`
--

CREATE TABLE `food_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `food_category`
--

INSERT INTO `food_category` (`id`, `title`, `description`) VALUES
(1, 'Chinese', 'just a chinese'),
(2, 'Italian', 'just an italian'),
(4, 'Nepalese', 'just a nepalese');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `infographics`
--

CREATE TABLE `infographics` (
  `info_id` int(11) NOT NULL,
  `numberinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labelinfo` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logomark` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `infographics`
--

INSERT INTO `infographics` (`info_id`, `numberinfo`, `labelinfo`, `logomark`) VALUES
(1, '79', 'Completed Projects', 'flaticon-briefcase'),
(2, '750', 'Happy Clients', 'flaticon-happy'),
(3, '36', 'Customer Services', 'flaticon-idea'),
(4, '20', 'Answered Questions', 'flaticon-customer-service');

-- --------------------------------------------------------

--
-- Table structure for table `item_orders`
--

CREATE TABLE `item_orders` (
  `id` int(11) NOT NULL,
  `food_name` varchar(255) DEFAULT NULL,
  `food_image` varchar(255) DEFAULT NULL,
  `food_price` double DEFAULT NULL,
  `food_quantity` int(11) DEFAULT NULL,
  `food_total_price` double DEFAULT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `item_orders`
--

INSERT INTO `item_orders` (`id`, `food_name`, `food_image`, `food_price`, `food_quantity`, `food_total_price`, `order_id`) VALUES
(4, 'Momo', '419a3-momo.jpg', 120, 3, 360, 8),
(5, 'Chaumin', '42048-maxresdefault.jpg', 100, 2, 200, 8),
(6, 'Momo', '419a3-momo.jpg', 120, 3, 360, 9),
(7, 'Chaumin', '42048-maxresdefault.jpg', 100, 2, 200, 9),
(8, 'Momo', '419a3-momo.jpg', 120, 3, 360, 10),
(9, 'Chaumin', '42048-maxresdefault.jpg', 100, 2, 200, 10),
(10, 'Momo', '419a3-momo.jpg', 120, 3, 360, 11),
(11, 'Chaumin', '42048-maxresdefault.jpg', 100, 2, 200, 11),
(12, 'Momo', '419a3-momo.jpg', 120, 3, 360, 12),
(13, 'Chaumin', '42048-maxresdefault.jpg', 100, 2, 200, 12),
(14, 'Momo', '419a3-momo.jpg', 120, 2, 240, 13),
(15, 'Chaumin', '42048-maxresdefault.jpg', 100, 1, 100, 13),
(16, 'Momo', '419a3-momo.jpg', 120, 2, 240, 14),
(17, 'Chaumin', '42048-maxresdefault.jpg', 100, 1, 100, 14),
(18, 'Momo', '419a3-momo.jpg', 120, 2, 240, 15);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL,
  `job_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening_date` date NOT NULL,
  `ending_date` date NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `job_title`, `job_description`, `opening_date`, `ending_date`, `page_slug`, `page_url`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1, 'Jr Web Developer', '<p>\n	Jr Web Developer</p>\n', '2021-01-01', '2021-01-31', 'jr-web-developer', 'http://localhost/neosoftware/jobs/jr-web-developer', '', '', ''),
(3, 'Web Designer', '<p>\n	Need to know bootstrap, Css and html.</p>\n', '2021-05-02', '2022-03-02', 'web-designer', 'http://localhost/impact/jobs/web-designer', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jobsubmission`
--

CREATE TABLE `jobsubmission` (
  `submissionid` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_id` int(11) NOT NULL,
  `cvupload` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateofsubmission` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `location_id` int(11) NOT NULL,
  `location_title` varchar(255) DEFAULT NULL,
  `location_description` varchar(255) DEFAULT NULL,
  `location_subtitle` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `location_title`, `location_description`, `location_subtitle`) VALUES
(1, 'New Baneshwor', 'Contact: 9809807090', 'Kathmandu, Nepal'),
(2, 'Koteshwor', 'Contact: 9809807090', 'Kathmandu, Nepal');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_title` varchar(250) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `link` varchar(250) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `url_type` enum('internal','external') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_title`, `menu_order`, `link`, `parent_id`, `url_type`) VALUES
(133, 'Home', 1, '', 0, 'internal'),
(134, 'Food', 3, 'food', 0, 'internal'),
(135, 'Cart', 4, 'cart', 0, 'internal'),
(132, 'Reservations', 2, 'reservation', 0, 'internal');

-- --------------------------------------------------------

--
-- Table structure for table `menu_category`
--

CREATE TABLE `menu_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu_category`
--

INSERT INTO `menu_category` (`id`, `title`, `description`) VALUES
(1, 'Chinese', '<p>\n	chinese description</p>\n'),
(2, 'Italian', '<p>\n	italian description</p>\n'),
(3, 'Indian', '<p>\n	Just an indian food</p>\n'),
(4, 'Nepalese', '<p>\n	just nepali food</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_price` int(11) DEFAULT NULL,
  `menu_category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu_item`
--

INSERT INTO `menu_item` (`id`, `item_name`, `item_price`, `menu_category_id`) VALUES
(1, 'Chicken Momo', 120, 1),
(2, 'Veg Momo', 80, 2),
(3, 'Buff Momo', 100, 1),
(4, 'Ramen', 50, 1),
(5, 'Curry', 100, 2),
(6, 'Dosa', 50, 3),
(7, 'Something', 40, 1),
(8, 'Daal Bhat', 120, 4);

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_date` date NOT NULL,
  `notice_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_image_alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_intro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_type` enum('notice','news') COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`notice_id`, `title`, `description`, `author`, `notice_date`, `notice_image`, `notice_image_alt`, `notice_intro`, `notice_type`, `page_slug`, `page_url`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(36, 'Test', '<p>\n	sdfdsgfdgnfnsfg fwgeg&nbsp; argadga wdf awd</p>\n', 'Rudra Rajbanshi', '2023-02-04', '51f60-lenovo-laptop-500x500.jpg', 'image', 'zdasfa', 'news', 'test', 'http://localhost/kontrast/news/test', 'asda', 'asdas', 'asdas'),
(37, 'Just a title', '<p>\n	asdasad weq&nbsp; qw d as</p>\n', 'Manjari Thapa', '2023-03-04', '5631a-dell.jpg', 'image', 'asdasd', 'news', 'just-a-title', 'http://localhost/kontrast/news/just-a-title', 'asda', 'asdas', 'asdas'),
(39, 'Avik Khadka', '<p>\n	scasc sf a fa sf af&nbsp;</p>\n', 'Avik Khadka', '2022-07-04', '62926-s8-.jpg', 'image', NULL, '', 'avik-khadka', 'http://localhost/kontrast/news/avik-khadka', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `officeCode` int(10) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `addressLine1` varchar(50) NOT NULL,
  `addressLine2` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) NOT NULL,
  `postalCode` varchar(15) NOT NULL,
  `territory` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`officeCode`, `city`, `phone`, `addressLine1`, `addressLine2`, `state`, `country`, `postalCode`, `territory`) VALUES
(1, 'San Francisco', '+1 650 219 4782', '100 Market Street', 'Suite 300', 'CA', 'USA', '94080', 'NA'),
(2, 'Boston', '+1 215 837 0825', '1550 Court Place', 'Suite 102', 'MA', 'USA', '02107', 'NA'),
(3, 'NYC', '+1 212 555 3000', '523 East 53rd Street', 'apt. 5A', 'NY', 'USA', '10022', 'NA'),
(4, 'Paris', '+33 14 723 4404', '43 Rue Jouffroy D', '', '', 'France', '75017', 'EMEA'),
(5, 'Tokyo', '+81 33 224 5000', '4-1 Kioicho', NULL, 'Chiyoda-Ku', 'Japan', '102-8578', 'Japan'),
(6, 'Sydney', '+61 2 9264 2451', '5-11 Wentworth Avenue', 'Floor #2', NULL, 'Australia', 'NSW 2010', 'APAC'),
(7, 'London', '+44 20 7877 2041', '25 Old Broad Street', 'Level 7', NULL, 'UK', 'EC2N 1HN', 'EMEA');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `status` enum('pending','processing','delivered','cancelled') NOT NULL,
  `shipping_name` varchar(255) DEFAULT NULL,
  `shipping_email` varchar(255) DEFAULT NULL,
  `shipping_address` varchar(255) DEFAULT NULL,
  `shipping_phone` varchar(255) DEFAULT NULL,
  `billing_name` varchar(255) DEFAULT NULL,
  `billing_email` varchar(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `billing_phone` varchar(255) DEFAULT NULL,
  `order_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `status`, `shipping_name`, `shipping_email`, `shipping_address`, `shipping_phone`, `billing_name`, `billing_email`, `billing_address`, `billing_phone`, `order_date`) VALUES
(10, 'delivered', 'Nabin Khanal', 'rudra.raj@gmail.com', 'Balkot', '9807906380', 'Rudra Rajbanshi', 'rudra.raj@gmail.com', 'Balkot', '9807906380', '2021-05-02 07:08:40'),
(11, 'pending', 'Rudra Rajbanshi', 'rudra.raj@gmail.com', 'Balkot', '9807906380', 'Rudra Rajbanshi', 'rudra.raj@gmail.com', 'Balkot', '9807906380', '2021-05-02 07:09:30'),
(12, 'pending', 'Nishan Khadka', 'rudra.raj@gmail.com', 'Balkot', '9807906380', 'Rudra Rajbanshi', 'rudra.raj@gmail.com', 'Balkot', '9807906380', '2021-05-02 10:08:58'),
(13, 'pending', 'NIshan', 'rudra.raj@gmail.com', 'Balkot', '9807906380', 'Rudra Rajbanshi', 'rudra.raj@gmail.com', 'Balkot', '9807906380', '2021-05-02 10:20:19'),
(14, 'pending', 'Rudra Rajbanshi', 'rudra.raj@gmail.com', 'Balkot', '9807906380', 'Rudra Rajbanshi', 'rudra.raj@gmail.com', 'Balkot', '9807906380', '2021-05-06 10:08:38'),
(15, 'pending', 'Rudra Rajbanshi', 'rudra.raj@gmail.com', 'Balkot', '9807906380', 'Rudra Rajbanshi', 'rudra.raj@gmail.com', 'Balkot', '9807906380', '2021-05-06 10:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_subtitle` varchar(250) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_url` varchar(250) NOT NULL,
  `featured_image` varchar(250) NOT NULL,
  `page_intro` text NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `og_title` varchar(255) NOT NULL,
  `og_url` varchar(255) NOT NULL,
  `og_image` varchar(255) NOT NULL,
  `og_type` varchar(255) NOT NULL,
  `og_description` text NOT NULL,
  `og_locale` varchar(255) NOT NULL,
  `schema_our` varchar(255) NOT NULL,
  `featured_image_alt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_title`, `page_subtitle`, `page_slug`, `page_content`, `page_url`, `featured_image`, `page_intro`, `meta_title`, `meta_description`, `meta_keywords`, `og_title`, `og_url`, `og_image`, `og_type`, `og_description`, `og_locale`, `schema_our`, `featured_image_alt`) VALUES
(43, 'About Us', 'About Us', 'about-us', '<div>\n	We offer a range of custom-designed web solutions that will help you achieve your short and long-term business goals: Social Media Management Services, SEO Services, App Store Marketing Services, E-commerce Marketing Services, Marketing Automation Services &ndash; you name it, we bring it to you. Our arsenal of innovative web marketing solutions caters to the requirements of companies of all shapes and sizes. Our result-oriented and to-the-budget solutions are a medley of elegant, functional designs and modern technology. No matter the nature of your business, when it comes to driving traffic, conversions, and measuring the effectiveness of campaigns, count on us to deliver every single time!</div>\n<div>\n	&nbsp;</div>\n<div>\n	Avani comprises a team of top digital strategists, artists and engineers that is always ready to deliver for you. Our services are designed in sync with the latest in technology and assure the very best results.</div>\n', 'http://localhost/avani/page/about-us', 'd21ba-avani-head.png', 'We offer a range of custom-designed web solutions that will help you achieve your short and long-term business goals: Social Media Management Services, SEO Services, App Store Marketing Services, E-commerce Marketing Services, Marketing Automation Services – you name it, we bring it to you. Our arsenal of innovative web marketing solutions caters to the requirements of companies of all shapes and sizes. Our result-oriented and to-the-budget solutions are a medley of elegant, functional designs and modern technology. No matter the nature of your business, when it comes to driving traffic, conversions, and measuring the effectiveness of campaigns, count on us to deliver every single time!\n\nAvani comprises a team of top digital strategists, artists and engineers that is always ready to deliver for you. Our services are designed in sync with the latest in technology and assure the very best results.', 'About Us', 'About Us', 'About Us', 'About Us', 'About Us', '26588-avani-head.png', 'About Us', 'About Us', 'About Us', 'About Us', 'About Us'),
(53, 'What We Do', 'What We Do', 'what-we-do', '<p>\n	What We Do</p>\n', 'http://localhost/avani/page/what-we-do', '24ac7-avani-head.png', 'What We Do', 'What We Do', 'What We Do', 'What We Do', 'What We Do', 'What We Do', 'b4ac4-avani-head.png', 'What We Do', 'What We Do', 'What We Do', 'What We Do', 'What We Do');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `customerNumber` int(11) NOT NULL,
  `checkNumber` varchar(50) NOT NULL,
  `paymentDate` datetime NOT NULL,
  `amount` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`customerNumber`, `checkNumber`, `paymentDate`, `amount`) VALUES
(103, 'HQ336336', '2004-10-19 00:00:00', 6066.78),
(103, 'JM555205', '2003-06-05 00:00:00', 14571.44),
(103, 'OM314933', '2004-12-18 00:00:00', 1676.14),
(112, 'BO864823', '2004-12-17 00:00:00', 14191.12),
(112, 'HQ55022', '2003-06-06 00:00:00', 32641.98),
(112, 'ND748579', '2004-08-20 00:00:00', 33347.88),
(114, 'GG31455', '2003-05-20 00:00:00', 45864.03),
(114, 'MA765515', '2004-12-15 00:00:00', 82261.22),
(114, 'NP603840', '2003-05-31 00:00:00', 7565.08),
(114, 'NR27552', '2004-03-10 00:00:00', 44894.74),
(119, 'DB933704', '2004-11-14 00:00:00', 19501.82),
(119, 'LN373447', '2004-08-08 00:00:00', 47924.19),
(119, 'NG94694', '2005-02-22 00:00:00', 49523.67),
(121, 'DB889831', '2003-02-16 00:00:00', 50218.95),
(121, 'FD317790', '2003-10-28 00:00:00', 1491.38),
(121, 'KI831359', '2004-11-04 00:00:00', 17876.32),
(121, 'MA302151', '2004-11-28 00:00:00', 34638.14),
(124, 'AE215433', '2005-03-05 00:00:00', 101244.59),
(124, 'BG255406', '2004-08-28 00:00:00', 85410.87),
(124, 'CQ287967', '2003-04-11 00:00:00', 11044.3),
(124, 'ET64396', '2005-04-16 00:00:00', 83598.04),
(124, 'HI366474', '2004-12-27 00:00:00', 47142.7),
(124, 'HR86578', '2004-11-02 00:00:00', 55639.66),
(124, 'KI131716', '2003-08-15 00:00:00', 111654.4),
(124, 'LF217299', '2004-03-26 00:00:00', 43369.3),
(124, 'NT141748', '2003-11-25 00:00:00', 45084.38),
(128, 'DI925118', '2003-01-28 00:00:00', 10549.01),
(128, 'FA465482', '2003-10-18 00:00:00', 24101.81),
(128, 'FH668230', '2004-03-24 00:00:00', 33820.62),
(128, 'IP383901', '2004-11-18 00:00:00', 7466.32),
(129, 'DM826140', '2004-12-08 00:00:00', 26248.78),
(129, 'ID449593', '2003-12-11 00:00:00', 23923.93),
(129, 'PI42991', '2003-04-09 00:00:00', 16537.85),
(131, 'CL442705', '2003-03-12 00:00:00', 22292.62),
(131, 'MA724562', '2004-12-02 00:00:00', 50025.35),
(131, 'NB445135', '2004-09-11 00:00:00', 35321.97),
(141, 'AU364101', '2003-07-19 00:00:00', 36251.03),
(141, 'DB583216', '2004-11-01 00:00:00', 36140.38),
(141, 'DL460618', '2005-05-19 00:00:00', 46895.48),
(141, 'HJ32686', '2004-01-30 00:00:00', 59830.55),
(141, 'ID10962', '2004-12-31 00:00:00', 116208.4),
(141, 'IN446258', '2005-03-25 00:00:00', 65071.26),
(141, 'JE105477', '2005-03-18 00:00:00', 120166.58),
(141, 'JN355280', '2003-10-26 00:00:00', 49539.37),
(141, 'JN722010', '2003-02-25 00:00:00', 40206.2),
(141, 'KT52578', '2003-12-09 00:00:00', 63843.55),
(141, 'MC46946', '2004-07-09 00:00:00', 35420.74),
(141, 'MF629602', '2004-08-16 00:00:00', 20009.53),
(141, 'NU627706', '2004-05-17 00:00:00', 26155.91),
(144, 'IR846303', '2004-12-12 00:00:00', 36005.71),
(144, 'LA685678', '2003-04-09 00:00:00', 7674.94),
(145, 'CN328545', '2004-07-03 00:00:00', 4710.73),
(145, 'ED39322', '2004-04-26 00:00:00', 28211.7),
(145, 'HR182688', '2004-12-01 00:00:00', 20564.86),
(145, 'JJ246391', '2003-02-20 00:00:00', 53959.21),
(146, 'FP549817', '2004-03-18 00:00:00', 40978.53),
(146, 'FU793410', '2004-01-16 00:00:00', 49614.72),
(146, 'LJ160635', '2003-12-10 00:00:00', 39712.1),
(148, 'BI507030', '2003-04-22 00:00:00', 44380.15),
(148, 'DD635282', '2004-08-11 00:00:00', 2611.84),
(148, 'KM172879', '2003-12-26 00:00:00', 105743),
(148, 'ME497970', '2005-03-27 00:00:00', 3516.04),
(151, 'BF686658', '2003-12-22 00:00:00', 58793.53),
(151, 'GB852215', '2004-07-26 00:00:00', 20314.44),
(151, 'IP568906', '2003-06-18 00:00:00', 58841.35),
(151, 'KI884577', '2004-12-14 00:00:00', 39964.63),
(157, 'HI618861', '2004-11-19 00:00:00', 35152.12),
(157, 'NN711988', '2004-09-07 00:00:00', 63357.13),
(161, 'BR352384', '2004-11-14 00:00:00', 2434.25),
(161, 'BR478494', '2003-11-18 00:00:00', 50743.65),
(161, 'KG644125', '2005-02-02 00:00:00', 12692.19),
(161, 'NI908214', '2003-08-05 00:00:00', 38675.13),
(166, 'BQ327613', '2004-09-16 00:00:00', 38785.48),
(166, 'DC979307', '2004-07-07 00:00:00', 44160.92),
(166, 'LA318629', '2004-02-28 00:00:00', 22474.17),
(167, 'ED743615', '2004-09-19 00:00:00', 12538.01),
(167, 'GN228846', '2003-12-03 00:00:00', 85024.46),
(171, 'GB878038', '2004-03-15 00:00:00', 18997.89),
(171, 'IL104425', '2003-11-22 00:00:00', 42783.81),
(172, 'AD832091', '2004-09-09 00:00:00', 1960.8),
(172, 'CE51751', '2004-12-04 00:00:00', 51209.58),
(172, 'EH208589', '2003-04-20 00:00:00', 33383.14),
(173, 'GP545698', '2004-05-13 00:00:00', 11843.45),
(173, 'IG462397', '2004-03-29 00:00:00', 20355.24),
(175, 'IO448913', '2003-11-19 00:00:00', 24879.08),
(175, 'PI15215', '2004-07-10 00:00:00', 42044.77),
(177, 'AU750837', '2004-04-17 00:00:00', 15183.63),
(177, 'CI381435', '2004-01-19 00:00:00', 47177.59),
(181, 'CM564612', '2004-04-25 00:00:00', 22602.36),
(175, 'CITI3434344', '2005-05-19 00:00:00', 28500.78),
(209, 'BOAF82044', '2005-05-03 00:00:00', 35157.75),
(181, 'GQ132144', '2003-01-30 00:00:00', 5494.78),
(181, 'OH367219', '2004-11-16 00:00:00', 44400.5),
(186, 'AE192287', '2005-03-10 00:00:00', 23602.9),
(186, 'AK412714', '2003-10-27 00:00:00', 37602.48),
(186, 'KA602407', '2004-10-21 00:00:00', 34341.08),
(187, 'AM968797', '2004-11-03 00:00:00', 52825.29),
(187, 'BQ39062', '2004-12-08 00:00:00', 47159.11),
(187, 'KL124726', '2003-03-27 00:00:00', 48425.69),
(189, 'BO711618', '2004-10-03 00:00:00', 17359.53),
(189, 'NM916675', '2004-03-01 00:00:00', 32538.74),
(198, 'FI192930', '2004-12-06 00:00:00', 9658.74),
(198, 'HQ920205', '2003-07-06 00:00:00', 6036.96),
(198, 'IS946883', '2004-09-21 00:00:00', 5858.56),
(201, 'DP677013', '2003-10-20 00:00:00', 23908.24),
(201, 'OO846801', '2004-06-15 00:00:00', 37258.94),
(202, 'HI358554', '2003-12-18 00:00:00', 36527.61),
(202, 'IQ627690', '2004-11-08 00:00:00', 33594.58),
(204, 'GC697638', '2004-08-13 00:00:00', 51152.86),
(204, 'IS150005', '2004-09-24 00:00:00', 4424.4),
(205, 'GL756480', '2003-12-04 00:00:00', 3879.96),
(205, 'LL562733', '2003-09-05 00:00:00', 50342.74),
(205, 'NM739638', '2005-02-06 00:00:00', 39580.6),
(209, 'ED520529', '2004-06-21 00:00:00', 4632.31),
(209, 'PH785937', '2004-05-04 00:00:00', 36069.26),
(211, 'BJ535230', '2003-12-09 00:00:00', 45480.79),
(216, 'BG407567', '2003-05-09 00:00:00', 3101.4),
(216, 'ML780814', '2004-12-06 00:00:00', 24945.21),
(216, 'MM342086', '2003-12-14 00:00:00', 40473.86),
(219, 'BN17870', '2005-03-02 00:00:00', 3452.75),
(219, 'BR941480', '2003-10-18 00:00:00', 4465.85),
(227, 'MQ413968', '2003-10-31 00:00:00', 36164.46),
(227, 'NU21326', '2004-11-02 00:00:00', 53745.34),
(233, 'II180006', '2004-07-01 00:00:00', 22997.45),
(233, 'JG981190', '2003-11-18 00:00:00', 16909.84),
(239, 'NQ865547', '2004-03-15 00:00:00', 80375.24),
(240, 'IF245157', '2004-11-16 00:00:00', 46788.14),
(240, 'JO719695', '2004-03-28 00:00:00', 24995.61),
(242, 'AF40894', '2003-11-22 00:00:00', 33818.34),
(242, 'HR224331', '2005-06-03 00:00:00', 12432.32),
(242, 'KI744716', '2003-07-21 00:00:00', 14232.7),
(249, 'IJ399820', '2004-09-19 00:00:00', 33924.24),
(249, 'NE404084', '2004-09-04 00:00:00', 48298.99),
(250, 'EQ12267', '2005-05-17 00:00:00', 17928.09),
(250, 'HD284647', '2004-12-30 00:00:00', 26311.63),
(250, 'HN114306', '2003-07-18 00:00:00', 23419.47),
(256, 'EP227123', '2004-02-10 00:00:00', 5759.42),
(256, 'HE84936', '2004-10-22 00:00:00', 53116.99),
(259, 'EU280955', '2004-11-06 00:00:00', 61234.67),
(259, 'GB361972', '2003-12-07 00:00:00', 27988.47),
(260, 'IO164641', '2004-08-30 00:00:00', 37527.58),
(260, 'NH776924', '2004-04-24 00:00:00', 29284.42),
(276, 'EM979878', '2005-02-09 00:00:00', 27083.78),
(276, 'KM841847', '2003-11-13 00:00:00', 38547.19),
(276, 'LE432182', '2003-09-28 00:00:00', 41554.73),
(276, 'OJ819725', '2005-04-30 00:00:00', 29848.52),
(278, 'BJ483870', '2004-12-05 00:00:00', 37654.09),
(278, 'GP636783', '2003-03-02 00:00:00', 52151.81),
(278, 'NI983021', '2003-11-24 00:00:00', 37723.79),
(282, 'IA793562', '2003-08-03 00:00:00', 24013.52),
(282, 'JT819493', '2004-08-02 00:00:00', 35806.73),
(282, 'OD327378', '2005-01-03 00:00:00', 31835.36),
(286, 'DR578578', '2004-10-28 00:00:00', 47411.33),
(286, 'KH910279', '2004-09-05 00:00:00', 43134.04),
(298, 'AJ574927', '2004-03-13 00:00:00', 47375.92),
(298, 'LF501133', '2004-09-18 00:00:00', 61402),
(299, 'AD304085', '2003-10-24 00:00:00', 36798.88),
(299, 'NR157385', '2004-09-05 00:00:00', 32260.16),
(311, 'DG336041', '2005-02-15 00:00:00', 46770.52),
(311, 'FA728475', '2003-10-06 00:00:00', 32723.04),
(311, 'NQ966143', '2004-04-25 00:00:00', 16212.59),
(314, 'LQ244073', '2004-08-09 00:00:00', 45352.47),
(314, 'MD809704', '2004-03-03 00:00:00', 16901.38),
(319, 'HL685576', '2004-11-06 00:00:00', 42339.76),
(319, 'OM548174', '2003-12-07 00:00:00', 36092.4),
(320, 'GJ597719', '2005-01-18 00:00:00', 8307.28),
(320, 'HO576374', '2003-08-20 00:00:00', 41016.75),
(320, 'MU817160', '2003-11-24 00:00:00', 52548.49),
(321, 'DJ15149', '2003-11-03 00:00:00', 85559.12),
(321, 'LA556321', '2005-03-15 00:00:00', 46781.66),
(323, 'AL493079', '2005-05-23 00:00:00', 75020.13),
(323, 'ES347491', '2004-06-24 00:00:00', 37281.36),
(323, 'HG738664', '2003-07-05 00:00:00', 2880),
(323, 'PQ803830', '2004-12-24 00:00:00', 39440.59),
(324, 'DQ409197', '2004-12-13 00:00:00', 13671.82),
(324, 'FP443161', '2003-07-07 00:00:00', 29429.14),
(324, 'HB150714', '2003-11-23 00:00:00', 37455.77),
(328, 'EN930356', '2004-04-16 00:00:00', 7178.66),
(328, 'NR631421', '2004-05-30 00:00:00', 31102.85),
(333, 'HL209210', '2003-11-15 00:00:00', 23936.53),
(333, 'JK479662', '2003-10-17 00:00:00', 9821.32),
(333, 'NF959653', '2005-03-01 00:00:00', 21432.31),
(334, 'CS435306', '2005-01-27 00:00:00', 45785.34),
(334, 'HH517378', '2003-08-16 00:00:00', 29716.86),
(334, 'LF737277', '2004-05-22 00:00:00', 28394.54),
(339, 'AP286625', '2004-10-24 00:00:00', 23333.06),
(339, 'DA98827', '2003-11-28 00:00:00', 34606.28),
(344, 'AF246722', '2003-11-24 00:00:00', 31428.21),
(344, 'NJ906924', '2004-04-02 00:00:00', 15322.93),
(347, 'DG700707', '2004-01-18 00:00:00', 21053.69),
(347, 'LG808674', '2003-10-24 00:00:00', 20452.5),
(350, 'BQ602907', '2004-12-11 00:00:00', 18888.31),
(350, 'CI471510', '2003-05-25 00:00:00', 50824.66),
(350, 'OB648482', '2005-01-29 00:00:00', 1834.56),
(353, 'CO351193', '2005-01-10 00:00:00', 49705.52),
(353, 'ED878227', '2003-07-21 00:00:00', 13920.26),
(353, 'GT878649', '2003-05-21 00:00:00', 16700.47),
(353, 'HJ618252', '2005-06-09 00:00:00', 46656.94),
(357, 'AG240323', '2003-12-16 00:00:00', 20220.04),
(357, 'NB291497', '2004-05-15 00:00:00', 36442.34),
(362, 'FP170292', '2004-07-11 00:00:00', 18473.71),
(362, 'OG208861', '2004-09-21 00:00:00', 15059.76),
(363, 'HL575273', '2004-11-17 00:00:00', 50799.69),
(363, 'IS232033', '2003-01-16 00:00:00', 10223.83),
(363, 'PN238558', '2003-12-05 00:00:00', 55425.77),
(379, 'CA762595', '2005-02-12 00:00:00', 28322.83),
(379, 'FR499138', '2003-09-16 00:00:00', 32680.31),
(379, 'GB890854', '2004-08-02 00:00:00', 12530.51),
(381, 'BC726082', '2004-12-03 00:00:00', 12081.52),
(381, 'CC475233', '2003-04-19 00:00:00', 1627.56),
(381, 'GB117430', '2005-02-03 00:00:00', 14379.9),
(381, 'MS154481', '2003-08-22 00:00:00', 1128.2),
(382, 'CC871084', '2003-05-12 00:00:00', 35826.33),
(382, 'CT821147', '2004-08-01 00:00:00', 6419.84),
(382, 'PH29054', '2004-11-27 00:00:00', 42813.83),
(385, 'BN347084', '2003-12-02 00:00:00', 20644.24),
(385, 'CP804873', '2004-11-19 00:00:00', 15822.84),
(385, 'EK785462', '2003-03-09 00:00:00', 51001.22),
(386, 'DO106109', '2003-11-18 00:00:00', 38524.29),
(386, 'HG438769', '2004-07-18 00:00:00', 51619.02),
(398, 'AJ478695', '2005-02-14 00:00:00', 33967.73),
(398, 'DO787644', '2004-06-21 00:00:00', 22037.91),
(398, 'KB54275', '2004-11-29 00:00:00', 48927.64),
(406, 'HJ217687', '2004-01-28 00:00:00', 49165.16),
(406, 'NA197101', '2004-06-17 00:00:00', 25080.96),
(412, 'GH197075', '2004-07-25 00:00:00', 35034.57),
(412, 'PJ434867', '2004-04-14 00:00:00', 31670.37),
(415, 'ER54537', '2004-09-28 00:00:00', 31310.09),
(424, 'KF480160', '2004-12-07 00:00:00', 25505.98),
(424, 'LM271923', '2003-04-16 00:00:00', 21665.98),
(424, 'OA595449', '2003-10-31 00:00:00', 22042.37),
(447, 'AO757239', '2003-09-15 00:00:00', 6631.36),
(447, 'ER615123', '2003-06-25 00:00:00', 17032.29),
(447, 'OU516561', '2004-12-17 00:00:00', 26304.13),
(448, 'FS299615', '2005-04-18 00:00:00', 27966.54),
(448, 'KR822727', '2004-09-30 00:00:00', 48809.9),
(450, 'EF485824', '2004-06-21 00:00:00', 59551.38),
(452, 'ED473873', '2003-11-15 00:00:00', 27121.9),
(452, 'FN640986', '2003-11-20 00:00:00', 15130.97),
(452, 'HG635467', '2005-05-03 00:00:00', 8807.12),
(455, 'HA777606', '2003-12-05 00:00:00', 38139.18),
(455, 'IR662429', '2004-05-12 00:00:00', 32239.47),
(456, 'GJ715659', '2004-11-13 00:00:00', 27550.51),
(456, 'MO743231', '2004-04-30 00:00:00', 1679.92),
(458, 'DD995006', '2004-11-15 00:00:00', 33145.56),
(458, 'NA377824', '2004-02-06 00:00:00', 22162.61),
(458, 'OO606861', '2003-06-13 00:00:00', 57131.92),
(462, 'ED203908', '2005-04-15 00:00:00', 30293.77),
(462, 'GC60330', '2003-11-08 00:00:00', 9977.85),
(462, 'PE176846', '2004-11-27 00:00:00', 48355.87),
(471, 'AB661578', '2004-07-28 00:00:00', 9415.13),
(471, 'CO645196', '2003-12-10 00:00:00', 35505.63),
(473, 'LL427009', '2004-02-17 00:00:00', 7612.06),
(473, 'PC688499', '2003-10-27 00:00:00', 17746.26),
(475, 'JP113227', '2003-12-09 00:00:00', 7678.25),
(475, 'PB951268', '2004-02-13 00:00:00', 36070.47),
(484, 'GK294076', '2004-10-26 00:00:00', 3474.66),
(484, 'JH546765', '2003-11-29 00:00:00', 47513.19),
(486, 'BL66528', '2004-04-14 00:00:00', 5899.38),
(486, 'HS86661', '2004-11-23 00:00:00', 45994.07),
(486, 'JB117768', '2003-03-20 00:00:00', 25833.14),
(487, 'AH612904', '2003-09-28 00:00:00', 29997.09),
(487, 'PT550181', '2004-02-29 00:00:00', 12573.28),
(489, 'OC773849', '2003-12-04 00:00:00', 22275.73),
(489, 'PO860906', '2004-01-31 00:00:00', 7310.42),
(495, 'BH167026', '2003-12-26 00:00:00', 59265.14),
(495, 'FN155234', '2004-05-14 00:00:00', 6276.6),
(496, 'EU531600', '2005-05-25 00:00:00', 30253.75),
(496, 'MB342426', '2003-07-16 00:00:00', 32077.44),
(496, 'MN89921', '2004-12-31 00:00:00', 52166),
(233, 'BOFA23232', '2005-05-20 00:00:00', 29070.38),
(398, 'JPMR4544', '2005-05-18 00:00:00', 615.45),
(406, 'BJMPR4545', '2005-04-23 00:00:00', 12190.85);

-- --------------------------------------------------------

--
-- Table structure for table `productlines`
--

CREATE TABLE `productlines` (
  `productLine` varchar(50) NOT NULL,
  `textDescription` varchar(4000) DEFAULT NULL,
  `htmlDescription` mediumtext DEFAULT NULL,
  `image` mediumblob DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productlines`
--

INSERT INTO `productlines` (`productLine`, `textDescription`, `htmlDescription`, `image`) VALUES
('Vintage Cars', 'Our Vintage Car models realistically portray automobiles produced from the early 1900s through the 1940s. Materials used include Bakelite, diecast, plastic and wood. Most of the replicas are in the 1:18 and 1:24 scale sizes, which provide the optimum in detail and accuracy. Prices range from $30.00 up to $180.00 for some special limited edition replicas. All models include a certificate of authenticity from their manufacturers and come fully assembled and ready for display in the home or office.', NULL, NULL),
('Ships', 'The perfect holiday or anniversary gift for executives, clients, friends, and family. These handcrafted model ships are unique, stunning works of art that will be treasured for generations! They come fully assembled and ready for display in the home or office. We guarantee the highest quality, and best value.', NULL, NULL),
('Trains', 'Model trains are a rewarding hobby for enthusiasts of all ages. Whether you\'re looking for collectible wooden trains, electric streetcars or locomotives, you\'ll find a number of great choices for any budget within this category. The interactive aspect of trains makes toy trains perfect for young children. The wooden train sets are ideal for children under the age of 5.', NULL, NULL),
('Planes', 'Unique, diecast airplane and helicopter replicas suitable for collections, as well as home, office or classroom decorations. Models contain stunning details such as official logos and insignias, rotating jet engines and propellers, retractable wheels, and so on. Most come fully assembled and with a certificate of authenticity from their manufacturers.', NULL, NULL),
('Motorcycles', 'Our motorcycles are state of the art replicas of classic as well as contemporary motorcycle legends such as Harley Davidson, Ducati and Vespa. Models contain stunning details such as official logos, rotating wheels, working kickstand, front suspension, gear-shift lever, footbrake lever, and drive chain. Materials used include diecast and plastic. The models range in size from 1:10 to 1:50 scale and include numerous limited edition and several out-of-production vehicles. All models come fully assembled and ready for display in the home or office. Most include a certificate of authenticity.', NULL, NULL),
('Classic Cars', 'Attention car enthusiasts: Make your wildest car ownership dreams come true. Whether you are looking for classic muscle cars, dream sports cars or movie-inspired miniatures, you will find great choices in this category. These replicas feature superb attention to detail and craftsmanship and offer features such as working steering system, opening forward compartment, opening rear trunk with removable spare wheel, 4-wheel independent spring suspension, and so on. The models range in size from 1:10 to 1:24 scale and include numerous limited edition and several out-of-production vehicles. All models include a certificate of authenticity from their manufacturers and come fully assembled and ready for display in the home or office.', NULL, NULL),
('Trucks and Buses', 'The Truck and Bus models are realistic replicas of buses and specialized trucks produced from the early 1920s to present. The models range in size from 1:12 to 1:50 scale and include numerous limited edition and several out-of-production vehicles. Materials used include tin, diecast and plastic. All models include a certificate of authenticity from their manufacturers and are a perfect ornament for the home and office.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_description`, `featured_image`, `product_category_id`, `meta_title`, `meta_description`, `meta_keywords`, `page_slug`) VALUES
(3, 'Development of Neo Software Website', '<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis leo eget enim volutpat malesuada. Aliquam in elit orci. Donec vel feugiat justo. Integer accumsan auctor leo sed laoreet. Duis erat diam, mollis sed sodales eu, maximus non nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus finibus porttitor ipsum at vestibulum. Nam in hendrerit metus, at placerat massa. Sed eget imperdiet odio. Morbi pretium consequat mauris, non finibus diam interdum vel. Duis sollicitudin elit ligula, a eleifend nulla pellentesque id. Phasellus convallis dolor ornare, aliquet ipsum vitae, vestibulum odio. Fusce sit amet eros ac quam mattis posuere at eu neque.</p>\n<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Etiam placerat velit at porttitor semper. In scelerisque egestas odio quis volutpat. Nullam a sem non lacus suscipit porttitor. Aliquam erat volutpat. In vitae lectus ac eros cursus molestie eu vel est. Etiam id luctus ante. Vivamus nec fringilla nunc. Proin tempus hendrerit tempus. Cras convallis tempor quam eu cursus. Phasellus sit amet enim sed risus fringilla elementum nec quis sapien. Vivamus cursus nisl a quam dictum pretium. Maecenas et neque vestibulum mi convallis tristique. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n', '8e048-project2.jpg', 2, 'Test title', 'test', 'test', 'development-of-neo-software-website');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `product_category_id` int(11) NOT NULL,
  `product_category_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`product_category_id`, `product_category_name`, `featured_image`, `meta_title`, `meta_description`, `meta_keywords`, `page_slug`) VALUES
(2, 'Web Application Development', '', 'Test title', 'test', 'test', 'web-application-development'),
(3, 'Mobile Application Development', '', '', '', '', 'mobile-application-development');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `project_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_image_alt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_category_id` int(11) NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_homepage` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_date` date DEFAULT NULL,
  `client` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `award` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_title`, `project_description`, `project_image`, `project_image_alt`, `project_link`, `project_category_id`, `meta_title`, `meta_description`, `meta_keywords`, `page_slug`, `is_homepage`, `project_date`, `client`, `award`) VALUES
(4, 'WOTa Money Save', '<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis leo eget enim volutpat malesuada. Aliquam in elit orci. Donec vel feugiat justo. Integer accumsan auctor leo sed laoreet. Duis erat diam, mollis sed sodales eu, maximus non nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus finibus porttitor ipsum at vestibulum. Nam in hendrerit metus, at placerat massa. Sed eget imperdiet odio. Morbi pretium consequat mauris, non finibus diam interdum vel. Duis sollicitudin elit ligula, a eleifend nulla pellentesque id. Phasellus convallis dolor ornare, aliquet ipsum vitae, vestibulum odio. Fusce sit amet eros ac quam mattis posuere at eu neque.</p>\n<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Etiam placerat velit at porttitor semper. In scelerisque egestas odio quis volutpat. Nullam a sem non lacus suscipit porttitor. Aliquam erat volutpat. In vitae lectus ac eros cursus molestie eu vel est. Etiam id luctus ante. Vivamus nec fringilla nunc. Proin tempus hendrerit tempus. Cras convallis tempor quam eu cursus. Phasellus sit amet enim sed risus fringilla elementum nec quis sapien. Vivamus cursus nisl a quam dictum pretium. Maecenas et neque vestibulum mi convallis tristique. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n', '6e548-1.jpg', '', 'https://www.google.com', 5, 'WOTa Money Save', 'WOTa Money Save', 'WOTa Money Save', 'wota-money-save', 'yes', '2021-01-03', 'US Embassy', 'Best Software of the year'),
(5, 'WOTa Money Save', '<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis leo eget enim volutpat malesuada. Aliquam in elit orci. Donec vel feugiat justo. Integer accumsan auctor leo sed laoreet. Duis erat diam, mollis sed sodales eu, maximus non nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus finibus porttitor ipsum at vestibulum. Nam in hendrerit metus, at placerat massa. Sed eget imperdiet odio. Morbi pretium consequat mauris, non finibus diam interdum vel. Duis sollicitudin elit ligula, a eleifend nulla pellentesque id. Phasellus convallis dolor ornare, aliquet ipsum vitae, vestibulum odio. Fusce sit amet eros ac quam mattis posuere at eu neque.</p>\n<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Etiam placerat velit at porttitor semper. In scelerisque egestas odio quis volutpat. Nullam a sem non lacus suscipit porttitor. Aliquam erat volutpat. In vitae lectus ac eros cursus molestie eu vel est. Etiam id luctus ante. Vivamus nec fringilla nunc. Proin tempus hendrerit tempus. Cras convallis tempor quam eu cursus. Phasellus sit amet enim sed risus fringilla elementum nec quis sapien. Vivamus cursus nisl a quam dictum pretium. Maecenas et neque vestibulum mi convallis tristique. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n', '904f1-2.jpg', '', 'https://www.youtube.com', 5, 'WOTa Money Save', 'WOTa Money Save', 'WOTa Money Save', 'wota-money-save', 'yes', '2021-01-07', 'Dubai Holidays', ''),
(6, 'WOTa Money Save', '<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis leo eget enim volutpat malesuada. Aliquam in elit orci. Donec vel feugiat justo. Integer accumsan auctor leo sed laoreet. Duis erat diam, mollis sed sodales eu, maximus non nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus finibus porttitor ipsum at vestibulum. Nam in hendrerit metus, at placerat massa. Sed eget imperdiet odio. Morbi pretium consequat mauris, non finibus diam interdum vel. Duis sollicitudin elit ligula, a eleifend nulla pellentesque id. Phasellus convallis dolor ornare, aliquet ipsum vitae, vestibulum odio. Fusce sit amet eros ac quam mattis posuere at eu neque.</p>\n<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Etiam placerat velit at porttitor semper. In scelerisque egestas odio quis volutpat. Nullam a sem non lacus suscipit porttitor. Aliquam erat volutpat. In vitae lectus ac eros cursus molestie eu vel est. Etiam id luctus ante. Vivamus nec fringilla nunc. Proin tempus hendrerit tempus. Cras convallis tempor quam eu cursus. Phasellus sit amet enim sed risus fringilla elementum nec quis sapien. Vivamus cursus nisl a quam dictum pretium. Maecenas et neque vestibulum mi convallis tristique. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n', '424fd-3.jpg', '', '', 5, 'WOTa Money Save', 'WOTa Money Save', 'WOTa Money Save', 'wota-money-save', 'yes', '2021-01-11', 'Bon Holidays', ''),
(7, 'WOTa Money Save', '<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis leo eget enim volutpat malesuada. Aliquam in elit orci. Donec vel feugiat justo. Integer accumsan auctor leo sed laoreet. Duis erat diam, mollis sed sodales eu, maximus non nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus finibus porttitor ipsum at vestibulum. Nam in hendrerit metus, at placerat massa. Sed eget imperdiet odio. Morbi pretium consequat mauris, non finibus diam interdum vel. Duis sollicitudin elit ligula, a eleifend nulla pellentesque id. Phasellus convallis dolor ornare, aliquet ipsum vitae, vestibulum odio. Fusce sit amet eros ac quam mattis posuere at eu neque.</p>\n<p font-size:=\"\" open=\"\" style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: \">\n	Etiam placerat velit at porttitor semper. In scelerisque egestas odio quis volutpat. Nullam a sem non lacus suscipit porttitor. Aliquam erat volutpat. In vitae lectus ac eros cursus molestie eu vel est. Etiam id luctus ante. Vivamus nec fringilla nunc. Proin tempus hendrerit tempus. Cras convallis tempor quam eu cursus. Phasellus sit amet enim sed risus fringilla elementum nec quis sapien. Vivamus cursus nisl a quam dictum pretium. Maecenas et neque vestibulum mi convallis tristique. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n', '162b3-4.jpg', '', '', 5, 'WOTa Money Save', 'WOTa Money Save', 'WOTa Money Save', 'wota-money-save', 'no', '2021-01-21', 'Music Awards 2011', 'Great Award'),
(8, 'Project 1', '<p>\n	just a project</p>\n', 'c24f3-dell.jpg', '', 'link.project', 6, 'p1', 'p11111111', 'p11', 'project-1', 'yes', '2022-04-04', 'rudra', 'nothing');

-- --------------------------------------------------------

--
-- Table structure for table `project_category`
--

CREATE TABLE `project_category` (
  `project_category_id` int(11) NOT NULL,
  `project_category_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image_alt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_category`
--

INSERT INTO `project_category` (`project_category_id`, `project_category_name`, `featured_image`, `featured_image_alt`, `meta_title`, `meta_description`, `meta_keywords`, `page_slug`) VALUES
(5, 'SAP Softwares', '', '', 'Test title', 'test', 'test', 'sap-softwares'),
(6, 'Category 1', '15392-saurya.jpg', '', 'c1', 'c111111111', 'c11', 'category-1'),
(7, 'Category 3', 'b9246-buddha.jpg', 'image alt', 'c1', '', '', 'category-3'),
(8, 'Category 4', '00711-buddha.jpg', 'image alt', '', '', '', 'category-4');

-- --------------------------------------------------------

--
-- Table structure for table `project_gallery`
--

CREATE TABLE `project_gallery` (
  `gallery_id` int(11) NOT NULL,
  `gallery_title` varchar(255) NOT NULL,
  `gallery_description` varchar(255) NOT NULL,
  `gallery_image` varchar(255) NOT NULL,
  `gallery_image_alt` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project_gallery`
--

INSERT INTO `project_gallery` (`gallery_id`, `gallery_title`, `gallery_description`, `gallery_image`, `gallery_image_alt`, `project_id`) VALUES
(1, 'gallery 1', '', 'd561b-buddha.jpg', 'buddha alt', 8),
(2, 'gallery 2', 'gallery 2 description', 'c4374-dell.jpg', 'alt', 8);

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `resource_id` int(9) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `reservation_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `no_of_guests` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`reservation_id`, `name`, `email`, `phone`, `location`, `date`, `time`, `no_of_guests`) VALUES
(9, 'Rudra Rajbanshi', 'rudra.raj@gmail.com', '9807906380', 'New Baneshwor', '2021-04-29', '05:18', 4);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_icon` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_image_alt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_image_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_image_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `og_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `og_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `og_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `og_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `og_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `og_locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_title`, `service_description`, `service_icon`, `service_image`, `service_image_alt`, `service_image_title`, `service_image_description`, `og_title`, `og_url`, `og_image`, `og_type`, `og_description`, `og_locale`, `meta_title`, `meta_description`, `meta_key`) VALUES
(1, 'The eye of the company', 'Well versed with media scenario of the country. Suggests with the best media plan structure. Specialized individuals for all ATL plans. Well versed with media scenario of the country. Suggests with the best media plan structure. Specialized individuals for all ATL plans', 'fa fa-eye', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 'the brain of the company', 'Well versed with media scenario of the country. Suggests with the best media plan structure. Specialized individuals for all ATL plans. Well versed with media scenario of the country. Suggests with the best media plan structure. Specialized individuals for all ATL plans', 'fa fa-brain', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 'the heart of the company', 'Well versed with media scenario of the country. Suggests with the best media plan structure. Specialized individuals for all ATL plans. Well versed with media scenario of the country. Suggests with the best media plan structure. Specialized individuals for all ATL plans', 'fa fa-heartbeat', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_title` varchar(250) NOT NULL,
  `slider_subtitle` text NOT NULL,
  `slider_topcaptain` varchar(250) NOT NULL,
  `slider_link` varchar(250) NOT NULL,
  `slider_img_alt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_image`, `slider_title`, `slider_subtitle`, `slider_topcaptain`, `slider_link`, `slider_img_alt`) VALUES
(5, '8b0be-buddha.jpg', 'Buddha Air', '<p>\n	asdasdas</p>\n', 'asdas', 'asdas', 'asda'),
(6, 'bde27-dell.jpg', 'DEll', '<p>\n	sdasda</p>\n', 'dsd', 'sdfs', 'sdfs'),
(7, '588bd-saurya.jpg', 'Saurya Air', '<p>\n	asfdsfvsdsdvsss&nbsp; wefsdf esdf sd&nbsp;</p>\n', 'asdas', 'saurya air', 'saurya air');

-- --------------------------------------------------------

--
-- Table structure for table `socialmedia`
--

CREATE TABLE `socialmedia` (
  `socialmedia_id` int(11) NOT NULL,
  `socialmedia_title` varchar(250) NOT NULL,
  `socialmedia_icon` varchar(250) NOT NULL,
  `socialmedia_image` varchar(250) NOT NULL,
  `socialmedia_link` varchar(250) NOT NULL,
  `socialmedia_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `socialmedia`
--

INSERT INTO `socialmedia` (`socialmedia_id`, `socialmedia_title`, `socialmedia_icon`, `socialmedia_image`, `socialmedia_link`, `socialmedia_order`) VALUES
(1, 'Facebook', 'fa fa-facebook', 'b401d-facebook_icon.png', 'https://www.facebook.com/', 1),
(2, 'Twitter', 'fa fa-youtube', '36a13-youtube_img.png', 'https://www.youtube.com/', 2),
(3, 'Instagram', 'fa fa-instagram', 'ec6bf-instagram_icon.jpg', 'https://www.instagram.com/', 3);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_order` int(11) DEFAULT NULL,
  `facebook_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `instagram_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`team_id`, `name`, `designation`, `info`, `image`, `team_order`, `facebook_url`, `twitter_url`, `instagram_url`) VALUES
(22, 'Edward Eric Jr', 'Business & Financial Expert', '<p>\n	<span style=\"color: rgb(102, 102, 102); font-family: &quot;Work Sans&quot;, sans-serif; font-size: 14px;\">Business &amp; Financial Expert</span></p>\n', '55365-expert1.jpg', 1, '', '', ''),
(23, 'Tom Holland', 'Logistic & Communication Expert', '<p>\n	Tom Holland</p>\n', '31575-expert2.jpg', 2, '', '', ''),
(24, 'Laura Erakovic', 'Consumer Market Expert', '<p>\n	<span style=\"color: rgb(102, 102, 102); font-family: &quot;Work Sans&quot;, sans-serif; font-size: 14px;\">Consumer Market Expert</span></p>\n', '2d767-expert3.jpg', 3, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_alt_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonial_id`, `testimonial_title`, `testimonial_description`, `testimonial_image`, `testimonial_alt_image`) VALUES
(8, 'Bobs Hanley', 'great experience working there', 'd8eac-testimonial1.jpg', 'asd'),
(9, 'Brain  Betthalyn', 'Too much to learn', 'f16c0-img2.jpg', 'dfg'),
(10, 'John Hanley', 'working hard to get there', '30616-testimonial2.jpg', 'zxc'),
(11, 'Sagun Siwakoti', 'Testimonial Tester', '196b6-avani-head.png', 'cvb');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$DjDF6D.5fZkJs0P9HmJjheT7COqUo95d7lrX6lKTrVdmhUPtJODSq', '', 'info@kontrast.com.np', '', NULL, NULL, NULL, 1268889823, 1620290433, 1, 'Admin', 'istrator', 'ADMIN', '9841546900'),
(2, '::1', 'rudra.raj@gmail.com', '$2y$08$C66FtLOFNRnaaquejbFTvuIKV55IL8fbIpZWK9hM88HTsKuLwjmMS', NULL, 'rudra.raj@gmail.com', NULL, NULL, NULL, NULL, 1619956779, 1620296151, 1, 'Rudra', 'Rajbanshi', 'Balkot', '9807906380'),
(3, '::1', 'rudra.raj.1@gmail.com', '$2y$08$0Th66gEiVGyBPovc2aTsVuNWloPlsvdss7TfcAjGh6OOIujzBl1kS', NULL, 'rudra.raj.1@gmail.com', NULL, NULL, NULL, NULL, 1620295009, NULL, 1, 'Rudra', 'Rajbanshi', 'Balkot', '9807906380');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(6, 1, 1),
(7, 2, 2),
(8, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `whatwedo`
--

CREATE TABLE `whatwedo` (
  `whatwedo_id` int(11) NOT NULL,
  `whatwedo_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `whatwedo_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `whatwe_do_image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `whatwedo_image_alt` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `whatwedo`
--

INSERT INTO `whatwedo` (`whatwedo_id`, `whatwedo_title`, `whatwedo_description`, `whatwe_do_image`, `whatwedo_image_alt`, `page_slug`) VALUES
(1, 'Campaigns', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac tortor eu enim mattis egestas. Nunc faucibus sit amet ipsum non congue. Morbi condimentum dignissim ligula in viverra.', '6cd97-campaigns.jpg', '', ''),
(2, 'Events', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac tortor eu enim mattis egestas. Nunc faucibus sit amet ipsum non congue. Morbi condimentum dignissim ligula in viverra.', 'c72cd-campaigns.jpg', '', ''),
(3, 'TVC', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac tortor eu enim mattis egestas. Nunc faucibus sit amet ipsum non congue. Morbi condimentum dignissim ligula in viverra.', '313a2-campaigns.jpg', '', ''),
(4, 'Jingle', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac tortor eu enim mattis egestas. Nunc faucibus sit amet ipsum non congue. Morbi condimentum dignissim ligula in viverra.', '0e4e4-campaigns.jpg', '', ''),
(5, 'Print', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac tortor eu enim mattis egestas. Nunc faucibus sit amet ipsum non congue. Morbi condimentum dignissim ligula in viverra.', '72b62-campaigns.jpg', '', ''),
(6, 'Digital', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac tortor eu enim mattis egestas. Nunc faucibus sit amet ipsum non congue. Morbi condimentum dignissim ligula in viverra.', 'bb01a-campaigns.jpg', '', ''),
(8, 'Branding', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac tortor eu enim mattis egestas. Nunc faucibus sit amet ipsum non congue. Morbi condimentum dignissim ligula in viverra.', 'e81d7-campaigns.jpg', '', ''),
(9, 'Media Buy', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac tortor eu enim mattis egestas. Nunc faucibus sit amet ipsum non congue. Morbi condimentum dignissim ligula in viverra.', '9fc71-campaigns.jpg', '', ''),
(10, 'title 1', 'what we do', '74eec-buddha.jpg', 'alt', 'title-1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`aboutus_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customerNumber`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employeeNumber`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD UNIQUE KEY `folder_id` (`folder_id`),
  ADD UNIQUE KEY `folder_name` (`folder_name`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`food_id`);

--
-- Indexes for table `food_category`
--
ALTER TABLE `food_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infographics`
--
ALTER TABLE `infographics`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `item_orders`
--
ALTER TABLE `item_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `jobsubmission`
--
ALTER TABLE `jobsubmission`
  ADD PRIMARY KEY (`submissionid`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `menu_category`
--
ALTER TABLE `menu_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`officeCode`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`customerNumber`,`checkNumber`);

--
-- Indexes for table `productlines`
--
ALTER TABLE `productlines`
  ADD PRIMARY KEY (`productLine`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`product_category_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `project_category`
--
ALTER TABLE `project_category`
  ADD PRIMARY KEY (`project_category_id`);

--
-- Indexes for table `project_gallery`
--
ALTER TABLE `project_gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`resource_id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`reservation_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `socialmedia`
--
ALTER TABLE `socialmedia`
  ADD PRIMARY KEY (`socialmedia_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `whatwedo`
--
ALTER TABLE `whatwedo`
  ADD PRIMARY KEY (`whatwedo_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `aboutus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customerNumber` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=497;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `employeeNumber` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1703;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `folder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `food_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `food_category`
--
ALTER TABLE `food_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `infographics`
--
ALTER TABLE `infographics`
  MODIFY `info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `item_orders`
--
ALTER TABLE `item_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jobsubmission`
--
ALTER TABLE `jobsubmission`
  MODIFY `submissionid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `menu_category`
--
ALTER TABLE `menu_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `officeCode` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `product_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `project_category`
--
ALTER TABLE `project_category`
  MODIFY `project_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `project_gallery`
--
ALTER TABLE `project_gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
  MODIFY `resource_id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `reservation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `socialmedia`
--
ALTER TABLE `socialmedia`
  MODIFY `socialmedia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `whatwedo`
--
ALTER TABLE `whatwedo`
  MODIFY `whatwedo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
