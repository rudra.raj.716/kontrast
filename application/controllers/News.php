<?php defined('BASEPATH') or exit('No direct script access allowed');

class News extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->data = new stdClass();
		$this->load->model('Menu_Model');
	}

	public function single($slug)
	{
		$data['notice'] = $this->db->select('*')->from('notice')->where('page_slug', $slug)->get()->row_array();
		$data['menus'] = $this->Menu_Model->get_menus();
		$this->template->title('Kontrast Reataurang')
		->set_layout('frontend/singlegrid.php')
		->build('frontend/blog-single.php', $data);
	}
}
