<?php defined('BASEPATH') or exit('No direct script access allowed');

class Backend extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->data = new stdClass();
	}

	public function dashboard()
	{
		$this->template->title('Dashboard')
			->set_layout('backend/admin.php')
			->build('admin/dashboard.php', $this->data);
	}

	public function pages()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('page');
			$crud->set_subject('Page');
			$crud->field_type('page_slug', 'hidden');
			$crud->field_type('page_url', 'hidden');
			$crud->unset_texteditor('meta_description', 'meta_keywords', 'og_description', 'featured_image_description');
			$crud->columns('page_title', 'page_slug', 'page_url');
			$crud->set_field_upload('featured_image', 'assets/uploads/');
			$crud->set_field_upload('og_image', 'assets/uploads/');
			$crud->unset_texteditor('page_intro');
			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['page_title']);
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['page_title']);
				return $post_array;
			});

			$output = $crud->render();

			$this->template->title('Pages')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function menu($parent_id = 0)
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('menu');
			$crud->where('parent_id', $parent_id);
			$crud->set_subject('Menu');
			$crud->order_by('menu_order', 'ASC');
			$crud->add_action('Childrens', '', 'backend/menu', 'fa fa-child');

			$output = $crud->render();

			$this->template->title('Dashboard')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function slider()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('slider');
			$crud->set_subject('Slider');
			$crud->set_field_upload('slider_image', 'assets/uploads');
			$output = $crud->render();

			$this->template->title('Slider')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function aboutus()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('about_us');
			$crud->set_subject('About Us');
			$crud->unset_add();
			$crud->unset_delete();
			$crud->set_field_upload('aboutus_image', 'assets/uploads');
			$crud->unset_texteditor('aboutus_description');
			$output = $crud->render();
			$this->template->title('Award')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function contact()
	{
		if ($_POST) {

			$from =  $this->input->post('email');  // User email pass here
			$subject = $this->input->post('subject');
			$name = $this->input->post('name');
			$phone = $this->input->post('phone');
			$to = 'rudra.raj.617@gmail.com';              // Pass here your mail id

			$emailContent = '';
			$emailContent .= '<tr><td style="height:20px"></td></tr>';
			$emailContent .= "<strong>" . $name . " (" . $phone . ")" . "</strong>" . "<br> From: " .$from . "<br>";
			$emailContent .= $this->input->post('message');  //   Post message available here

			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'ssl://smtp.gmail.com';
			$config['smtp_port']    = '465';
			$config['smtp_timeout'] = '60';

			// $config['smtp_user']    = '';    //Important
			// $config['smtp_pass']    = '';  //Important

			$config['charset']    = 'utf-8';
			$config['newline']    = "\r\n";
			$config['mailtype'] = 'html'; // or html
			$config['validation'] = TRUE; // bool whether to validate email or not 

			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($emailContent);
			$this->email->send();

			$this->session->set_flashdata('msg', "Mail has been sent successfully");
			$this->session->set_flashdata('msg_class', 'alert-success');
			redirect(base_url());
		}
	}

	public function faq()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('faq');
			$output = $crud->render();
			$this->template->title('FAQ')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function project_category()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('project_category');

			$crud->set_field_upload('featured_image', 'assets/uploads/');
			$crud->unset_texteditor('meta_description', 'meta_keywords');

			$crud->field_type('page_slug', 'hidden');

			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['project_category_name']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['project_category_name']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$output = $crud->render();
			$this->template->title('Project Category')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function project()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('projects');
			$crud->unset_texteditor('meta_description', 'meta_keywords');
			$crud->set_relation('project_category_id', 'project_category', 'project_category_name');
			$crud->set_field_upload('project_image', 'assets/uploads/');
			$crud->field_type('page_slug', 'hidden');
			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['project_title']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['project_title']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$output = $crud->render();
			$this->template->title('Projects')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function clients()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('clients');
			$crud->set_subject('Clients');
			$crud->set_field_upload('client_image', 'assets/uploads');
			$output = $crud->render();
			$this->template->title('Clients')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function whatwedo()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('whatwedo');
			$crud->set_subject('What We Do');
			$crud->set_field_upload('whatwe_do_image', 'assets/uploads');
			$crud->unset_texteditor('whatwedo_description');
			$crud->field_type('page_slug', 'hidden');
			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['whatwedo_title']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['whatwedo_title']);
				/*$post_array['page_url'] = base_url().'page/'.$post_array['page_slug'];*/
				return $post_array;
			});

			$output = $crud->render();
			$this->template->title('Award')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function project_gallery()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('project_gallery');
			$crud->set_subject('Project Gallery');
			$crud->set_field_upload('gallery_image', 'assets/uploads');

			$crud->set_relation('project_id', 'projects', 'project_title');
			$output = $crud->render();

			$this->template->title('Gallery')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function notice()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('notice');
			$crud->set_subject('Notices');
			$crud->columns('title', 'author', 'notice_date', 'notice_image', 'notice_intro', 'notice_type');
			$crud->order_by('notice_date', 'DESC');
			$crud->set_field_upload('notice_image', 'assets/uploads');
			$crud->unset_texteditor('notice_intro');
			$crud->unset_texteditor('meta_description', 'meta_keywords');
			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['title']);
				$post_array['page_url'] = base_url() . 'news/' . $post_array['page_slug'];
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['title']);
				$post_array['page_url'] = base_url() . 'news/' . $post_array['page_slug'];
				return $post_array;
			});

			//$crud->set_field_upload('slider_image','assets/uploads');

			$output = $crud->render();
			$this->template->title('Notice')
				->set_layout('backend/admin.php')
				->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
		//notice_id, title, description, notice_date, notice_file, notice_type, page_url, page_slug
	}

	public function menu_category()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('menu_category');
			$crud->set_subject('Menu Category');
			$output = $crud->render();

			$this->template->title('Menu Category')
			->set_layout('backend/admin.php')
			->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function menu_item()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('menu_item');
			$crud->set_subject('Menu Item');

			$crud->set_relation('menu_category_id', 'menu_category', 'title');
			$output = $crud->render();

			$this->template->title('Menu Item')
			->set_layout('backend/admin.php')
			->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function reservations()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('reservations');
			$crud->set_subject('Reservations');
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$output = $crud->render();

			$this->template->title('Reservations')
			->set_layout('backend/admin.php')
			->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}


	public function food()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('food');
			$crud->set_subject('Food');
			$crud->set_field_upload('food_image', 'assets/uploads');
			$crud->set_relation('food_category_id', 'food_category', 'title');
			$crud->set_relation('location_id', 'location', 'location_title');

			$output = $crud->render();

			$this->template->title('Food')
			->set_layout('backend/admin.php')
			->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function location()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('location');
			$crud->set_subject('Location');
			$crud->set_field_upload('location_image', 'assets/uploads');
			$crud->field_type('page_slug', 'hidden');

			$crud->callback_before_insert(function ($post_array) {
				$post_array['page_slug'] = General_helper::slugify($post_array['location_title']);
				return $post_array;
			});

			$crud->callback_before_update(function ($post_array, $primarykey) {
				$post_array['page_slug'] = General_helper::slugify($post_array['location_title']);
				return $post_array;
			});

			$output = $crud->render();

			$this->template->title('Location')
			->set_layout('backend/admin.php')
			->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function food_category()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('food_category');
			$crud->set_subject('Food Category');
			$output = $crud->render();

			$this->template->title('Food Category')
			->set_layout('backend/admin.php')
			->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function orders()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('orders');
			$crud->set_subject('Orders');
			$crud->order_by('order_date', 'desc');
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_edit();
			$crud->unset_read();
			$crud->add_action('View Ordered Items', '', 'backend/itemsOrdered', 'fa fa-child');
			$output = $crud->render();

			$this->template->title('Food Category')
			->set_layout('backend/admin.php')
			->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function itemsOrdered($id)
	{
		$data['items'] = $this->db->select('*')->from('item_orders')->join('orders', 'orders.id = item_orders.order_id')->where('order_id',$id)->get()->result_array();
		$data['order_id'] = $id;

		$discount = $this->db->select('discount')->from('orders')->where('id', $id)->get()->row();
		$data['dis'] = $discount->discount;

		$this->template->title('Food Category')
		->set_layout('backend/admin.php')
		->build('admin/order_items.php', $data);
	}

	public function change_status($id)
	{
		$data = array(
			'status' => $this->input->post('status')
		);
		$this->db->where('id', $id);
		$this->db->update('orders', $data);

		redirect('backend/itemsOrdered/'.$id);
	}

	public function discount_coupon()
	{
		try {
			$crud = new grocery_CRUD();
			$crud->set_table('discount_coupon');
			$crud->set_subject('Discount Coupon');
			$output = $crud->render();

			$this->template->title('Discount Coupon')
			->set_layout('backend/admin.php')
			->build('gc_template.php', $output);
		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

}
