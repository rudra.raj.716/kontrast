<?php

use kcfinder\session;

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->data = new stdClass();
		$this->load->model('Page_Model');
		$this->load->model('Slider_Model');
		$this->load->model('Contact_Model');
		$this->load->model('Team_Model');
		$this->load->model('Notice_Model');
		$this->load->model('Service_Model');
		$this->load->model('Faq_Model');
		$this->load->model('Testimonial_Model');
		$this->load->model('Product_Model');
		$this->load->model('ProductCategory_Model');
		$this->load->helper('captcha');
		$this->load->model('Project_Model');
		$this->load->model('ProjectCategory_Model');
		$this->load->model('Menu_Model');
		$this->load->model('About_Model');
		$this->load->model('Notice_Model');
		$this->load->model('Menu_Category_Model');
		$this->load->model('Reservation_Model');
		$this->load->model('Food_Model');
		$this->load->model('Location_Model');
		$this->load->model('Order_Model');
		$this->load->library('cart');
		$this->load->library('session');
		$this->load->helper('url');
	}

	public function index()
	{
		$data = array();

		$data['about'] = $this->About_Model->get_aboutUs();
		$data['notices'] = $this->Notice_Model->get_notice();

		$items = $this->db->select('*')->from('menu_category')->join('menu_item', 'menu_category.id = menu_item.menu_category_id')->get()->result_array();

		$empty_array = [];
		foreach ($items as $key => $value) {
			$empty_array[$value['title']][] = $value;
		}
		$data['menu_categories'] = $empty_array;

		$socials = array();

		$data['socials'] = $socials;
		$this->template->title('Kontrast Restaurang')
			->set_layout('frontend/public.php')
			->build('frontend/home.php', $data);
	}

	public function news($slug)
	{
		$data['notice'] = $this->db->select('*')->from('notice')->where('page_slug', $slug)->get()->row_array();
		$data['menus'] = $this->Menu_Model->get_menus();
		$this->template->title('Kontrast Reataurang')
			->set_layout('frontend/singlegrid.php')
			->build('frontend/blog-single.php', $data);
	}

	public function reservation()
	{
		$data['menus'] = $this->Menu_Model->get_menus();
		$data['locations'] = $this->Location_Model->get_location();
		$this->template->title('Kontrast Restaurang')
			->set_layout('frontend/singlegrid.php')
			->build('frontend/reservation.php', $data);
	}

	public function reservation_store()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('time', 'Time', 'required');
		$this->form_validation->set_rules('no_of_guests', 'No of Guests', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('msg', "Failed to Submit your details, Please try again");
			$this->session->set_flashdata('msg_class', 'alert-danger');
			redirect(base_url('/home/reservation'));
		} else {
			$this->Reservation_Model->set_reservation();
			$this->session->set_flashdata('msg', "Your details have been submitted");
			$this->session->set_flashdata('msg_class', 'alert-success');
			redirect(base_url('/home/reservation'));
		}
	}

	public function food($id)
	{
		$data['location_id'] = $id;
		$loc_name = $this->db->select('location_title')->from('location')->where('location_id', $id)->get()->row();
		$data['location_name'] = $loc_name->location_title;

		$data['food_categories'] = $this->db->get('food_category')->result_array();

		$this->template->title('Kontrast Restaurang')
			->set_layout('frontend/singlegrid.php')
			->build('frontend/food.php', $data);
	}

	public function getFood($id, $location_id)
	{
		$category = $this->db->select('title')->from('food_category')->where('id', $id)->get()->result();
		$foods = $this->db->select('*')->from('food')->where('food_category_id', $id)->where('location_id', $location_id)->get()->result_array();
		echo json_encode([$foods, $category]);
	}

	public function getAllFood($id)
	{
		$allFoods = $this->db->select('*')->from('food')->where('location_id', $id)->get()->result_array();
		echo json_encode($allFoods);
	}

	public function setCart($id)
	{
		$food_item = $this->db->select('*')->from('food')->where('food_id', $id)->get()->row_array();
		// $data['menus'] = $this->Menu_Model->get_menus();

		$items = array(
			array(
				'id'      => $food_item['food_id'],
				'qty'     => 1,
				'price'   => $food_item['food_price'],
				'name'    => $food_item['food_title'],
				'image'    => $food_item['food_image'],

			)
		);

		$check = $this->cart->insert($items);

		if ($check) {
			$this->session->set_flashdata('msg', "Added to cart");
			$this->session->set_flashdata('msg_class', 'alert-success');
			redirect('home/food/' . $food_item['location_id']);
		} else {
			$this->session->set_flashdata('msg', "Failed to add to cart, Please try again");
			$this->session->set_flashdata('msg_class', 'alert-danger');
			redirect('home/food' . $food_item['location_id']);
		}
	}

	public function cart()
	{
		// $data['menus'] = $this->Menu_Model->get_menus();
		$data['items'] = $this->cart->contents();

		$this->template->title('Kontrast Restaurang')
			->set_layout('frontend/singlegrid.php')
			->build('frontend/cart.php', $data);
	}

	public function cartItemIncrease($id, $qty)
	{
		$this->cart->update(
			array(
				'rowid' => $id,
				'qty' => $qty + 1
			)
		);
		redirect('home/cart');
	}

	public function cartItemDecrease($id, $qty)
	{
		$this->cart->update(
			array(
				'rowid' => $id,
				'qty' => $qty - 1
			)
		);
		redirect('home/cart');
	}

	public function cartItemRemove($id)
	{
		$this->cart->update(
			array(
				'rowid' => $id,
				'qty' => 0
			)
		);
		redirect('home/cart');
	}

	public function orderForm()
	{
		$items = $this->cart->contents();

		$sum = 0;
		foreach ($items as $item) {
			$sum += $item['subtotal'];
		}
		$data['totalAmt'] = $sum;

		$this->template->title('Kontrast Restaurang')
			->set_layout('frontend/singlegrid.php')
			->build('frontend/order_form.php', $data);
	}

	public function getDiscount()
	{
		$discounts = $this->db->from('discount_coupon')->get()->result_array();
		echo json_encode($discounts);
	}

	public function orders()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('shipping_name', 'Shipping Name', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('msg', "Failed to Submit your details, Please try again");
			$this->session->set_flashdata('msg_class', 'alert-danger');
			redirect(base_url('/home/orderForm'));
		} else {
			$order_id = $this->Order_Model->set_order();
			$items = $this->cart->contents();

			foreach ($items as $item) {

				$data = array(
					'food_name' => $item['name'],
					'food_image' => $item['image'],
					'food_price' => $item['price'],
					'food_quantity' => $item['qty'],
					'food_total_price' => $item['subtotal'],
					'order_id' => $order_id,

				);
				$this->db->insert('item_orders', $data);
			}
			$this->session->set_flashdata('msg', "Your Order has been Confirmed");
			$this->session->set_flashdata('msg_class', 'alert-success');
			$this->cart->destroy();

			redirect(base_url('/home/cart'));
		}
	}

	public function myOrders()
	{
		$data['menus'] = $this->Menu_Model->get_menus();

		$user_id = Filter::current_user()->user_id;
		$data['orders'] = $this->db->select('*')->from('orders')->where('user_id', $user_id)->order_by("order_date", "desc")->get()->result_array();

		$this->template->title('Kontrast Restaurang')
			->set_layout('frontend/singlegrid.php')
			->build('frontend/my_orders.php', $data);
	}

	public function singleOrderView($id)
	{
		$data['menus'] = $this->Menu_Model->get_menus();
		$data['items'] = $this->db->select('*')->from('item_orders')->where('order_id', $id)->get()->result_array();

		$this->template->title('Kontrast Restaurang')
			->set_layout('frontend/singlegrid.php')
			->build('frontend/my_order_items.php', $data);
	}

	public function contact()
	{
		if ($_POST) {

			$from =  $this->input->post('email');  // User email pass here
			$subject = $this->input->post('subject');
			$name = $this->input->post('name');
			$phone = $this->input->post('phone');
			$to = 'rudra.raj.617@gmail.com';              // Pass here your mail id

			$emailContent = '';
			$emailContent .= '<tr><td style="height:20px"></td></tr>';
			$emailContent .= "<strong>" . $name . " (" . $phone . ")" . "</strong>" . "<br> From: " . $from . "<br>";
			$emailContent .= $this->input->post('message');  //   Post message available here

			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'ssl://smtp.gmail.com';
			$config['smtp_port']    = '465';
			$config['smtp_timeout'] = '60';

			// $config['smtp_user']    = '';    //Important
			// $config['smtp_pass']    = '';  //Important

			$config['charset']    = 'utf-8';
			$config['newline']    = "\r\n";
			$config['mailtype'] = 'html'; // or html
			$config['validation'] = TRUE; // bool whether to validate email or not 

			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($emailContent);
			$this->email->send();

			$this->session->set_flashdata('msg', "Mail has been sent successfully");
			$this->session->set_flashdata('msg_class', 'alert-success');
			redirect(base_url());
		}
	}
}
