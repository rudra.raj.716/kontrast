<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->data = new stdClass();
		$this->load->model('Product_Model');
		$this->load->model('ProductCategory_Model');
		$this->load->helper('captcha');
		$this->load->model('Project_Model');
		$this->load->model('ProjectCategory_Model');
		$this->load->model('Scoda_model');
	}

	public function index($slug)
	{	
		$this->data->page = (array)$this->db->select('*')->from('page')->where('page_slug',$slug)->get()->row();
		if(empty($this->data->page))
		{
			echo '404 page not found';
			die;
		}
		$this->template->title($this->data->page['page_title'])
	    			->set_layout('frontend/singlegrid.php')
	        		->build('frontend/page.php', $this->data);
	}




	
	

}