<?php defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->data = new stdClass();
		$this->load->library('form_validation');
		$this->load->model('Contact_Model');
		$this->load->helper('captcha');
		$this->load->library('session');
		//$this->load->model('Notice_Model');
	}


	public function index()
	{
		$this->data->aboutus = (array)$this->db->select('*')->from('page')->where('page_id', 43)->get()->row();
		$vals = array(
			'word'          => rand(1, 999999),
			'img_path'      => './assets/captcha/images/',
			'img_url'       => base_url('assets') . '/captcha/images/',
			'font_path'     => base_url('assets') . '/captcha/fonts/XYZ.ttf',
			'img_width'     => '150',
			'img_height'    => 30,
			'word_length'   => 8,
			'colors'        => array(
				'background'     => array(255, 255, 255),
				'border'         => array(255, 255, 255),
				'text'           => array(0, 0, 0),
				'grid'           => array(255, 75, 100)
			)
		);

		if ($_POST) {
			$from =  $this->input->post('email');  // User email pass here
			$subject = $this->input->post('subject');
			$name = $this->input->post('name');
			$phone = $this->input->post('phone');
			$to = 'rudra.raj.617@gmail.com';              // Pass here your mail id



			$to = 'info@avani.com.np';   
			$emailContent = '';           // Pass here your mail id


			$emailContent .= '<tr><td style="height:20px"></td></tr>';

			$emailContent .= "<strong>" . $name . "</strong>" . "<br>";

			$emailContent .= $this->input->post('message');  //   Post message available here

		

			$config['charset']    = 'utf-8';
			$config['newline']    = "\r\n";
			$config['mailtype'] = 'html'; // or html
			$config['validation'] = TRUE; // bool whether to validate email or not 



			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($emailContent);
			$status = $this->email->send();
			if ($status) {
				$this->session->set_flashdata('msg', "Mail has been sent successfully");
			} else {
				$this->session->set_flashdata('error', "Email could not be sent. Try Again.");
			}
			redirect(base_url() . 'home');
		}
		// $this->template->title('Avani Advertising')
		// 	->set_layout('frontend/singlegrid.php')
		// 	->build('frontend/contact.php', $this->data);
	}

	public function contact()
	{
		die;
		if ($_POST) {
			$from =  $this->input->post('email');  // User email pass here
			$subject = $this->input->post('subject');
			$name = $this->input->post('name');
			$to = 'avikkhadka10@gmail.com';              // Pass here your mail id
			$emailContent .= '<tr><td style="height:20px"></td></tr>';
			$emailContent .= "<strong>" . $name . "</strong>" . "<br>";
			$emailContent .= $this->input->post('message');  //   Post message available here
			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'ssl://smtp.gmail.com';
			$config['smtp_port']    = '465';
			$config['smtp_timeout'] = '60';

			$config['smtp_user']    = 'avikkhadka10@gmail.com';    //Important
			$config['smtp_pass']    = 'yaoomnoo03';  //Important

			$config['charset']    = 'utf-8';
			$config['newline']    = "\r\n";
			$config['mailtype'] = 'html'; // or html
			$config['validation'] = TRUE; // bool whether to validate email or not 



			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($emailContent);
			$this->email->send();

			$this->session->set_flashdata('msg', "Mail has been sent successfully");
			$this->session->set_flashdata('msg_class', 'alert-success');
			redirect('Contact/index', $data);
		}
	}

	public function check_captcha($string)
	{
		if ($string != $this->session->userdata('captcha_answer')) {
			$this->form_validation->set_message('check_captcha', 'captcha incorrect');
			return false;
		} else {
			return true;
		}
	}
}
