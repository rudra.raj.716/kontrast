<?php defined('BASEPATH') or exit('No direct script access allowed');

class Location extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        
    }

    public function index($slug)
    {
        $data['location'] = $this->db->get_where('location',array('location_id' => $slug))->row_array();

        $items = $this->db->select('*')->from('food_category')->join('food', 'food_category.id = food.food_category_id')->where('location_id',$slug)->get()->result_array();

        $empty_array = [];
        foreach ($items as $key => $value) {
            $empty_array[$value['title']][] = $value;
        }
        $data['menu_categories'] = $empty_array;

        $this->template->title('Kontrast Reataurang')
            ->set_layout('frontend/singlegrid.php')
            ->build('frontend/location_single.php', $data);
    }
}
