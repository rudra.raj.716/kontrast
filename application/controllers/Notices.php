<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notices extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->data = new stdClass();
		$this->load->model('Notice_Model');
	}

	public function all()
	{
		$this->data->aboutus = (array)$this->db->select('*')->from('page')->where('page_id', 43)->get()->row();
		$this->load->model('News_Model');
		$config['base_url'] = base_url('notices/');

		$config['total_rows'] = $this->Notice_Model->get_num_rows();
		
		$config['per_page'] = 10;
		$config['uri_segment'] = 2;
		 // Bootstrap 4 Pagination fix
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
    // $config['next_tag_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tag_close']  = '</span></li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tag_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close']  = '</span></li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(2) : 0;
		$this->data->notices = (array)$this->Notice_Model->get_notice($config["per_page"], $page);

		$this->template->title('Acharya Subas & Associates, Chartered Accountants')
		->set_layout('frontend/single.php')
		->build('frontend/notice.php', $this->data);
	}


	public function single($slug)
	{
		$this->data->aboutus = (array)$this->db->select('*')->from('page')->where('page_id', 43)->get()->row();
		$this->data->notice_single = (array)$this->db->select('*')->from('notice')->where('page_slug',$slug)->order_by('notice_date','DESC')->order_by('notice_id','DESC')->get()->row();
		$this->template->title('Acharya Subas & Associates, Chartered Accountants')
		->set_layout('frontend/single.php')
		->build('frontend/notice_single.php', $this->data);
	}
}