<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';

$route['blog'] = 'Scoda/notice';
$route['about'] ='Scoda/about_us';
$route['contact'] ='Contact';
$route['home'] ='Home/index';

$route['us']='Scoda/us';
$route['portfolio']='Scoda/portfolio';
$route['article/(:any)'] = 'Scoda/single_notice/$1';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['register'] = "home/register";
$route['program/(:any)'] = "program/index/$1";
$route['page/(:any)'] = "page/index/$1";
$route['academic-calendar'] = "page/events";
$route['management-committee'] = "page/committee";
$route['faculty/(:any)'] = "faculty/index/$1";
$route['news/faculty-news'] = "news/faculty_news";
$route['news/faculty-news/(:any)'] = "news/faculty_news/$1";
$route['news/(:any)'] = "news/single/$1";
$route['member/(:any)'] = "faculty/member/$1";
$route['notices/(:any)'] = "notices/all/$1";
$route['notices/(:any)/(:any)'] = "notices/all/$1/$2";
$route['notice-single/(:any)'] = "notices/single/$1";
$route['resources'] = "publications";
$route['casestudy'] = "home/casestudy";
$route['case/(:any)'] = "home/case/$1";
$route['awards'] = "home/awards";
$route['projects/(:any)'] = "home/projects/$1";


$route['location/(:any)'] = "location/index/$1";


