
<?php //pr($this->session->flashdata('errors'),1); ?>
<?php if($this->session->flashdata('errors')) { ?>
            <div class="alert alert-danger">
              <?php echo $this->session->flashdata('errors'); ?>
            </div>
<?php } ?>



<div class="box box-warning">
  <div class="box-header with-border">
      <h3 class="box-title pull-left">All Folders</h3>

  
       <a href="" class="pull-right btn btn-success"  data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add New Folder</a>
   
  </div>
            <!-- /.box-header -->
            <?php if(!empty($folders)) { ?>
  <div class="box-body">

  <?php foreach($folders as $f) { ?>
  		<div class="col-lg-3 parentofthumb">
	  		<div class="thumbnail">

         
		  		<a class="close" folderid="<?php echo $f->folder_id; ?>">&times;</a>
        
		  		<a href="<?php echo base_url(); ?>files/all_files/<?php echo $f->folder_id; ?>">
		  			<img src="<?php echo base_url(); ?>assets/backend/img/folder.ico" style="height:100px">
		  			
		  			<div class="caption" style="text-align: center;">
			          <p><?php echo $f->folder_name; ?></p>
			        </div>
		  		</a>
		  	</div>	
  		</div>

    <?php }  }?>
    
   </div>
</div>



<!-- Modal -->

<form name="" action="<?php echo base_url(); ?>files/make_folder/<?php echo $project_id; ?>" method="post">
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Folder</h4>
      </div>
      <div class="modal-body">
        	<div class="form-group">
        		<label>Folder Name : </label>
        		<input type="text" name="folder_name" class="form-control">
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</form>



<script>
$(function()
{
	var delete_url = "<?php echo base_url(); ?>files/delete_folder/";
 	$('.close').click(function()
  	{
      var result = confirm("Are You Sure, You Want to delete?");
      if(result)
      {
       var folderid = $(this).attr('folderid');
        $.post(delete_url + folderid);
        $(this).parents('.parentofthumb').remove();
      }
      return false;
  	});
})
	
</script>