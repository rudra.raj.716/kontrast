<div class="container">
    <div class="row">
        <div class="card">

            <?php
            if (!empty($items)) {
            ?>
                <div class="card">
                    <div class="card-header">
                        <h4><b>Ordered By: </b><?php echo $items[0]['billing_name'] ?></h4>
                        <h4><b>Email: </b><?php echo $items[0]['billing_email'] ?></h4>
                        <h4><b>Phone: </b><?php echo $items[0]['billing_phone'] ?></h4>
                        <h4><b>Address: </b><?php echo $items[0]['billing_address'] ?></h4>

                    </div>
                    <h4><a class="btn btn-default" href="<?php echo base_url('backend/orders') ?>">Back to list</a></h4>
                    <div class="text-center">
                        <h4><u><b>Items (<?php echo $items[0]['status'] ?>)</b></u></h4>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td><b>Food</b></td>
                                <td><b>Price</b></td>
                                <td><b>Quantity</b></td>
                                <td><b>Total</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sum = 0;
                            foreach ($items as $item) {
                                $sum += $item['food_total_price'];
                            ?>

                                <tr>
                                    <td>
                                        <img src="<?php echo base_url() ?>assets/uploads/<?php echo $item['food_image'] ?>" height="auto" width="150" alt="">
                                        <?php echo $item['food_name'] ?>
                                    </td>
                                    <td><?php echo $item['food_price'] ?></td>
                                    <td><?php echo $item['food_quantity'] ?></td>
                                    <td><?php echo $item['food_total_price'] ?></td>
                                </tr>

                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <span class="pull-right" style="margin-right:7%">
                        <h4>Net Total: <?php echo $sum ?></h4>
                        <h4>Discount: <?php echo $dis ?? '0' ?>%</h4>
                        <h4> Final Amount: <?php echo $sum - ($dis / 100 * $sum) ?> </h4>
                        <form action="<?php echo base_url('backend/change_status/' . $order_id); ?>" method="POST">
                            <div class="row col-md-12">
                                <div class="col-md-8">
                                    <select class="form-control" name="status">
                                        <?php if ($items[0]['status'] == 'pending') { ?>
                                            <option value="processing">Processing</option>
                                            <option value="delivered">Delivered</option>
                                            <option value="cancelled">Cancelled</option>
                                        <?php } ?>

                                        <?php if ($items[0]['status'] == 'processing') { ?>
                                            <option value="delivered">Delivered</option>
                                            <option value="cancelled">Cancelled</option>
                                        <?php } ?>

                                        <?php if ($items[0]['status'] == 'delivered') { ?>
                                            <option value="processing">Processing</option>
                                            <option value="cancelled">Cancelled</option>
                                        <?php } ?>

                                        <?php if ($items[0]['status'] == 'cancelled') { ?>
                                            <option value="processing">Processing</option>
                                            <option value="delivered">Delivered</option>
                                        <?php } ?>

                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input class="btn btn-primary btn-sm" type="submit" name="submit" value="Change Status">
                                </div>


                            </div>
                        </form>
                    </span>
                </div>
            <?php
            } else {
            ?>
                <div class="card text-center">
                    <div class="card-header">
                        <h3><i>No Items to Display</i></h3>
                    </div>
                </div>

            <?php
            }
            ?>
        </div>
    </div>
</div>