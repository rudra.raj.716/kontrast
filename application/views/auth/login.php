<?php $this->load->view('layouts/backend/partials/admin_header.php'); ?>
<body class="hold-transition login-page" style="background-color: #FFF" >
<div class="login-box">
  <div class="login-logo">
   <!-- <img src="<?php echo base_url(); ?>assets/img/construction-helmet.png" style="height:69px"> -->
   <br/><span style = 'font-size:16px;'><b>Kontrast Restaurang</b></span>
  </div>
        <?php if($message) { ?>
          <div class="alert alert-danger"><?php echo $message;?></div>
        <?php } ?>
            <div class="panel panel-default" >
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <?php echo form_open("auth/login");?>

                        <div class="form-group">
                             

                            <div class="col-lg-12">
                                <label>Email</label>
                                <?php echo form_input($identity);?>
                            </div>
                        </div>

                        <div class="form-group">
                         

                            <div class="col-md-12">
                              <label>Password</label>
                                 <?php echo form_input($password);?>
                            </div>
                        </div>

                        <div class="form-group" >
                            <div class="col-md-12 col-md-offset-0">
                                <button style = 'margin-top: 20px;' type="submit" class="btn btn-primary btn-block btn-construction">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
  </div>
</div>


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>