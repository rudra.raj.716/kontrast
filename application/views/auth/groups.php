<a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>auth/create_group"><i class="fa fa-plus" aria-hidden="true"></i> New Group</a>

<a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>auth/create_user"><i class="fa fa-plus" aria-hidden="true"></i> New User</a>
	<br/><br/>

<div class="box box-warning">
	<div class="box-header with-border">
              <h3 class="box-title">Manange Groups</h3>
     </div>
	<?php if(validation_errors()) { ?>
          <div class="alert alert-danger"><?php echo validation_errors();?></div>
 	<?php } ?>

	
	 <div class="box-body">
	<table class="table table-bordered table-striped dataTable">
		<tr>
			<th>Group Name</th>
			<th>Group Description</th>
			<th style="text-align:center">Actions</th>
		</tr>
		<?php foreach ($groups as $gr):?>
			<tr>
	            <td><?php echo htmlspecialchars($gr->name,ENT_QUOTES,'UTF-8');?></td>
	            <td><?php echo htmlspecialchars($gr->description,ENT_QUOTES,'UTF-8');?></td>

	           	<td>
	           	<?php if(!General_helper::is_default_group($gr->name))
	           	{ ?>
	           		<a href="<?php echo base_url(); ?>auth/edit_group/<?php echo $gr->id; ?>" class="btn btn-success btn-xs">Edit</a> 

	           	<?php if($gr->name != 'admin') {?>
	           		<a href="<?php echo base_url(); ?>auth/delete_group/<?php echo $gr->id; ?>" class="btn btn-danger btn-xs deletesure">Delete</a>

	           	<?php }  } ?>
	           	<?php if($gr->name != 'admin') {?>
	           	<a href="<?php echo base_url(); ?>auth/edit_permission/<?php echo $gr->id; ?>" class="btn btn-warning btn-xs">Edit Permission</a>
	           	<?php } ?>

	           	</td>
	         
			</tr>
		<?php endforeach;?>
	</table>

<!-- 	<p><?php echo anchor('auth/create_user', lang('index_create_user_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?></p> -->
	</div>

</div>