<!-- <a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>auth/create_group"><i class="fa fa-plus" aria-hidden="true"></i> New Group</a>
 -->
	<!-- <a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>auth/create_user"><i class="fa fa-plus" aria-hidden="true"></i> New User</a> -->
	<!-- <br/><br/> -->

	<?php if(validation_errors()) { ?>
          <div class="alert alert-danger"><?php echo validation_errors();?></div>
 <?php } ?>


	<div class="box box-warning">
	  <div class="box-header with-border">
	              <h3  class="box-title">Create New User</h3>
	     </div>
	    <div class="box-body">
<table class="table table-bordered table-striped dataTable">
	<tr>
		<th><?php echo lang('index_fname_th');?></th>
		<th><?php echo lang('index_lname_th');?></th>
		<th><?php echo lang('index_email_th');?></th>
		<th><?php echo lang('index_groups_th');?></th>
		<!-- <th><?php// echo lang('index_status_th');?></th> -->
		<th><?php echo lang('index_action_th');?></th>
	</tr>
	<?php foreach ($users as $user):?>
		<tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td>
				<?php foreach ($user->groups as $group):?>
					<?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8'); ?><br />
                <?php endforeach?>
			</td>
			<!-- <td><span class="label bg-green"><?php echo  ($user->active == 1 ? 'active' : 'not active'); ?></span></td> -->
			<td><!-- <a href="<?php //echo base_url(); ?>auth/edit_user/<?php //echo $user->id; ?>" class="btn btn-primary btn-xs">Edit</a>  -->
			<?php if($user->email != 'admin@admin.com') { ?>
				<a href="<?php echo base_url(); ?>auth/delete_user/<?php echo $user->id; ?>" class="btn btn-danger btn-xs">Delete</a>
			<?php } ?>
			</td>
		</tr>
	<?php endforeach;?>
</table>

<!-- <p><?php echo anchor('auth/create_user', lang('index_create_user_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?></p> -->
</div>