<?php if(validation_errors()) { ?>
          <div class="alert alert-danger"><?php echo validation_errors();?></div>
 <?php } ?>

 <div class="box box-warning">
  <div class="box-header with-border">
              <h3  class="box-title">Create New Group</h3>
     </div>
<div class="box-body">

<?php echo form_open("auth/create_group");?>

      <p>
            <?php echo lang('create_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            <?php echo lang('create_group_desc_label', 'description');?> <br />
            <?php echo form_input($description);?>
      </p>

      <p><input type="submit" value="Submit" class="btn btn-success"></p>

<?php echo form_close();?>

</div>