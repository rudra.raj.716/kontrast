<div class="ed_pagetitle">
      <div class="ed_img_overlay"></div>
      <div class="container">
            <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="page_title">
                              <h2>Register User</h2>
                        </div>
                  </div>

            </div>
      </div>
</div>

<div class="ed_transprentbg ed_toppadder40 ed_bottompadder40">
      <div class="container">
            <div class="row col-md-6">

                  <?php if (validation_errors()) { ?>
                        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
                  <?php } ?>

                  <?php if (null !== $this->session->flashdata('message')) { ?>
                        <div class="alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
                  <?php } ?>

                  <div class="box box-warning">
                        <div class="box-header with-border">
                              <h3 class="box-title">Register</h3>
                        </div>
                        <div class="box-body">
                              <?php echo form_open("auth/create_user"); ?>

                              <p>
                                    <?php echo lang('create_user_fname_label', 'first_name'); ?> <br />
                                    <?php echo form_input($first_name); ?>
                              </p>

                              <p>
                                    <?php echo lang('create_user_lname_label', 'last_name'); ?> <br />
                                    <?php echo form_input($last_name); ?>
                              </p>

                              <?php
                              if ($identity_column !== 'email') {
                                    echo '<p>';
                                    echo lang('create_user_identity_label', 'identity');
                                    echo '<br />';
                                    echo form_error('identity');
                                    echo form_input($identity);
                                    echo '</p>';
                              }
                              ?>

                              <p>
                                    <?php echo lang('create_user_company_label', 'company'); ?> <br />
                                    <?php echo form_input($company); ?>
                              </p>

                              <p>
                                    <?php echo lang('create_user_email_label', 'email'); ?> <br />
                                    <?php echo form_input($email); ?>
                              </p>

                              <p>
                                    <?php echo lang('create_user_phone_label', 'phone'); ?> <br />
                                    <?php echo form_input($phone); ?>
                              </p>

                              <p>
                                    <?php echo lang('create_user_password_label', 'password'); ?> <br />
                                    <?php echo form_input($password); ?>
                              </p>

                              <p>
                                    <?php echo lang('create_user_password_confirm_label', 'password_confirm'); ?> <br />
                                    <?php echo form_input($password_confirm); ?>
                              </p>


                              <p><input type="submit" class="btn btn-success" value="Submit"></p>

                              <?php echo form_close(); ?>
                        </div>
                  </div>
            </div>
      </div>
</div>