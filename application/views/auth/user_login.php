<div class="ed_pagetitle">
  <div class="ed_img_overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="page_title">
          <h2>Login</h2>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="ed_transprentbg ed_toppadder40 ed_bottompadder40">
  <div class="container">
    <div class="row col-md-6">


      <?php if (validation_errors()) { ?>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
      <?php } ?>

      <?php if (null !== $this->session->flashdata('message')) { ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('message'); ?></div>
      <?php } ?>

      <div class="box box-warning">

        <div class="box-body">

          <?php echo form_open("auth/user_login"); ?>

          <div class="form-group">
            <div class="col-lg-12">
              <label>Email</label>
              <?php echo form_input($identity); ?>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
              <label>Password</label>
              <?php echo form_input($password); ?>
            </div>
          </div>


          <div class="form-group">
            <div class="col-md-4" style="margin-top:2%">
              <button type="submit" class="btn btn-primary btn-block btn-construction">
                Login
              </button>
            </div>
          </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>