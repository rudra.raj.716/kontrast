<?php if($message) { ?>
          <div class="alert alert-danger"><?php echo $message;?></div>
 <?php } ?>
 <div class="box box-warning">
  <div class="box-header with-border">
              <h3  class="box-title">Edit Group</h3>
     </div>
<div class="box-body">

<?php echo form_open(current_url());?>

      <p>
            <?php echo lang('edit_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            <?php echo lang('edit_group_desc_label', 'description');?> <br />
            <?php echo form_input($group_description);?>
      </p>

      <p><input type="submit" class="btn btn-success" value="Submit"></p>
<?php echo form_close();?>
</div>
</div>