<?php
$pages = $this->db->from('page')->get()->result_array();

?>
<!-- Footer
================================================== -->
<footer class="main-footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-5">
        <h4>CONTACT INFO</h4>
        <p>Möllevångstorget 6B, 21 424 Malmö</p>
        <p>Phone: 040 303313</p>
      </div>
      <div class="col-lg-5">
        <h4>OPEN HOURS</h4>
        <p>Sun - Thu 11:30 - 23:00 | Kitchen close 22:00</p>
        <p>Fri - Sat 11:30 - 01:00 | Kitchen close 23:00</p>
      </div>
      <div class="col-lg-2">
        <h4>FOLLOW US</h4>
        <ul class="social-icon">
          <li><a href="#" class="fa fa-facebook wow fadeIn" data-wow-delay="0.9s"></a></li>
          <li><a href="#" class="fa fa-twitter wow fadeIn" data-wow-delay="0.9s"></a></li>
          <li><a href="#" class="fa fa-instagram wow fadeIn" data-wow-delay="0.9s"></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>

<footer class="copyright-footer">
  <div class="container copyright-footer-wrapper">
    <div class="row">
      <div class="col-md-6 col-sm-6 wow fadeInUp" data-wow-delay="1s">
        <p>Copyright © Kontrast Restaurang | All Rights Reserved.</p>
      </div>
      <div class="col-md-6 col-sm-6 wow fadeInUp text-right" data-wow-delay="1s">
        <p>
          <?php
          foreach ($pages as $page) {
          ?>
            <a href="<?php echo base_url() . 'page/' . $page['page_slug'] ?>"><?php echo $page['page_title'] ?> |</a>

          <?php
          }
          ?>
        </p>
      </div>
    </div>
  </div>
</footer>


<!-- Javascript 
================================================== -->
<script src="<?php echo base_url(); ?>assets/frontend/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/jquery.sticky.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/jquery.parallax.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/smoothscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/custom.js"></script>


</body>

</html>