<!-- Services Section -->
<?php $services = $this->db->select('*')->from('services')->get()->result(); ?>
<?php if(!empty($services)){ ?>
  <div class="services-section">
    <div class="auto-container">
      <div class="sec-title">
        <div class="title">our services</div>
        <h2><span>Best Solutions</span> For Your Business</h2>
      </div>
      <div class="inner-container">
        <div class="row g-0">

          <?php foreach($services as $key => $val){ ?>
          <!-- Service Block -->
          <div class="service-block col-lg-3 col-md-6 col-sm-12">
            <div class="inner-box">
              <div class="icon-box">
                <span class="icon <?php echo $val->service_icon; ?>"></span>
              </div>
              <h5><a><?php echo $val->service_title; ?></a></h5>
              <div class="text"><?php echo $val->service_description; ?></div>
             <!--  <a class="read-more" href="<?php echo base_url(); ?>assets/frontend/service-detail.html">More <span class="ti-angle-right"></span></a> -->
            </div>
          </div>
        <?php } ?>
        </div>
        
      </div>
    </div>
  </div>
  <!-- End Services Section -->
  <?php } ?>