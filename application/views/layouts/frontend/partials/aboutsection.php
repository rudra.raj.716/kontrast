
  <!-- About Section -->
  <?php 
  $aboutus = (array)$this->db->select('*')->from('page')->where('page_id', 43)->get()->row();
  ?>
  <div class="about-section">
    <div class="auto-container">
      <div class="inner-container">
        <div class="row align-items-center clearfix">
          <!-- Image Column -->
          <div class="image-column col-lg-6">
            <div class="about-image">
              <div class="about-inner-image">
                <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $aboutus['featured_image']; ?>" alt="about">
              </div>
            </div>
          </div>

          <!-- Content Column -->
          <div class="content-column col-lg-6 col-md-12 col-sm-12 mb-0">
            <div class="about-column">
              <div class="sec-title">
                <div class="title"><?php echo $aboutus['page_title']; ?></div>
                <h2><?php echo $aboutus['page_subtitle']; ?></h2>
              </div>
              <div class="text">
                <?php echo $aboutus['page_intro']; ?>
              </div>
             <!--  <div class="signature">Daniel <span>Daniel Jr</span></div> -->
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- End About Section -->