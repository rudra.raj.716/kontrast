<?php
$menus = $this->Menu_Model->get_menus();
$locations = $this->db->from('location')->get()->result_array();


?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="">
  <meta name="description" content="">

  <title>KONTRAST RESTAURANG</title>

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/et-line-font.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/style.css">

  <style>
    .dropdown {
      position: relative;
      display: inline-block;
    }

    .dropdown-content {
      display: none;
      position: absolute;
      background-color: #f9f9f9;
      min-width: 160px;
      width: 220px;
      /* box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2); */
      padding: 0 5px;
      /* z-index: 1; */
      list-style-type: none;
    }

    .dropdown:hover .dropdown-content {
      display: block;
    }
  </style>

</head>

<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

  <div class="preloader">

    <div class="sk-spinner sk-spinner-pulse"></div>

  </div>



  <!-- Navigation section
================================================== -->
  <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
        </button>
        <a href="#" class="navbar-brand"><img src="<?php echo base_url(); ?>assets/frontend/images/new-logo.png" class="img-fluid" alt="kontrast-logo"></a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url() ?>home" class="smoothScroll">Home</a></li>
          <li class="dropdown"><a href="#">Restaurant</i></a>
            <ul class="dropdown-content">
              <?php
              foreach ($locations as $location) {
              ?>
                <li class="sub"><a href="<?php echo base_url() . 'location/' . $location['location_id'] ?>"></i><?php echo $location['location_title'] ?></a></li>
              <?php
              }
              ?>
            </ul>
          </li>

          <li class="dropdown"><a href="#">Food</i></a>
            <ul class="dropdown-content">
              <?php
              foreach ($locations as $location) {
              ?>
                <li class="sub"><a href="<?php echo base_url() . 'home/food/' . $location['location_id'] ?>"></i><?php echo $location['location_title'] ?></a></li>
              <?php
              }
              ?>
            </ul>
          </li>

          <?php
          foreach ($menus as $menu) {
          ?>
            <li><a href="<?php echo base_url() ?>home/<?php echo $menu['link'] ?>" class="smoothScroll"><?php echo $menu['menu_title'] ?></a></li>
          <?php
          }
          ?>

          <?php
          $ci = get_instance();
          if ($ci->ion_auth->logged_in()) {
          ?>
            <li><a href="<?php echo base_url() ?>home/myOrders" class="smoothScroll">My Orders</a></li>
            <li><a href="" class="smoothScroll"><span class="badge badge-primary"><?php echo Filter::current_user()->first_name; ?></span></a></li>
            <li><a href="<?php echo base_url() ?>auth/logout" class="smoothScroll"><span class="badge badge-primary">Logout</span></a></li>
          <?php
          } else {
          ?>
            <li><a href="<?php echo base_url() ?>auth/user_login" class="smoothScroll"><span class="badge badge-primary">Login</span></a></li>
            <li><a href="<?php echo base_url() ?>auth/create_user" class="smoothScroll"><span class="badge badge-primary">Register</span></a></li>
          <?php
          }

          ?>

        </ul>
      </div>

    </div>
  </div>
  <?php if ($this->session->flashdata('msg')) { ?>
    <div class="alert alert-success" role="alert">
      <?php echo $this->session->flashdata('msg'); ?>

      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <?php } ?>