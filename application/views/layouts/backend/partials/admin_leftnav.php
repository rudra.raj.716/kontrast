<?php
$current_url = $this->uri->segment(1) . '/' . $this->uri->segment(2);
$main_url = $this->uri->segment(1);

?>

<aside class="main-sidebar">

  <section class="sidebar">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <!-- <li class="header">MAIN NAVIGATION</li> -->
      <li class="<?php if ($current_url == 'dashboard/') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("/backend/dashboard"); ?>">
          <span>Dashboard</span>
        </a>
      </li>

      <li class="<?php if ($current_url == 'backend/menu' || $current_url == 'backend/pages' || $current_url == 'auth/change_password' || $current_url == 'backend/aboutus') {
                    echo 'active';
                  } ?>">

        <a href="#">
          <span>Settings</span>
          <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">


          <li class="<?php if ($current_url == 'backend/menu') {
                        echo 'active';
                      } ?>"><a href="<?php echo base_url('backend/menu'); ?>">Nav Menus</a></li>

          <li class="<?php if ($current_url == 'backend/aboutus') {
                        echo 'active';
                      } ?>"><a href="<?php echo base_url('backend/aboutus'); ?>">About Us</a></li>

          <li class="<?php if ($current_url == 'backend/pages') {
                        echo 'active';
                      } ?>"><a href="<?php echo base_url('backend/pages'); ?>">Pages</a></li>


          <li class="<?php if ($current_url == 'auth/change_password') {
                        echo 'active';
                      } ?>"><a href="<?php echo base_url('auth/change_password'); ?>">Change Password</a></li>
        </ul>
      </li>

      <li class="<?php if ($current_url == 'backend/menu_item' || $current_url == 'backend/menu_category') {
                    echo 'active';
                  } ?>">
        <a href="#">
          <span>Menu Category</span>
          <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if ($current_url == 'backend/menu_category') {
                        echo 'active';
                      } ?> treeview">
            <a href="<?php echo base_url("/backend/menu_category"); ?>">
              <span>Menu Category</span>
            </a>
          </li>

          <li class="<?php if ($current_url == 'backend/menu_item') {
                        echo 'active';
                      } ?> treeview">
            <a href="<?php echo base_url("/backend/menu_item"); ?>">
              <span>Menu Items</span>
            </a>
          </li>
        </ul>
      </li>

      <li class="<?php if ($current_url == 'backend/reservations') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("backend/reservations"); ?>">
          <span>Reservations</span>
        </a>
      </li>

      <li class="<?php if ($current_url == 'backend/food_category' || $current_url == 'backend/food') {
                    echo 'active';
                  } ?>">
        <a href="#">
          <span>Food Category</span>
          <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if ($current_url == 'backend/food_category') {
                        echo 'active';
                      } ?> treeview">
            <a href="<?php echo base_url("/backend/food_category"); ?>">
              <span>Food Category</span>
            </a>
          </li>

          <li class="<?php if ($current_url == 'backend/food') {
                        echo 'active';
                      } ?> treeview">
            <a href="<?php echo base_url("/backend/food"); ?>">
              <span>Food Items</span>
            </a>
          </li>
        </ul>
      </li>

      <li class="<?php if ($current_url == 'backend/location') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("backend/location"); ?>">
          <span>Location</span>
        </a>
      </li>

      <li class="<?php if ($current_url == 'backend/notice') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("/backend/notice"); ?>">
          <span>Notice</span>
        </a>
      </li>

      <li class="<?php if ($current_url == 'backend/orders') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("/backend/orders"); ?>">
          <span>Orders</span>
        </a>
      </li>

      <li class="<?php if ($current_url == 'backend/discount_coupon') {
                    echo 'active';
                  } ?> treeview">
        <a href="<?php echo base_url("/backend/discount_coupon"); ?>">
          <span>Discount Coupon</span>
        </a>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>