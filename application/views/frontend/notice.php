

  <!--=== page-title-section start ===-->
  <section class="title-hero-bg blog-cover-bg" data-stellar-background-ratio="0.2">
    <div class="container">
      <div class="page-title text-center">
        <h1>Notice</h1>
        <h4 class="text-uppercase mt-30 white-color">Checkout Our Latest Posts</h4>
      </div>
    </div>
  </section>
  <!--=== page-title-section end ===-->

  <!--=== Blogs Start ======-->
  <section class="pt-100 pb-100">
    
    <?php
        foreach($users as $user)
          {
          ?>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="post">
            <div class="post-img"> <a href="<?=base_url().'article/'.$user['page_slug']?>"><img class="img-responsive" src="<?php echo base_url();?>assets/uploads/<?php echo $user['notice_image'];?>" alt=""/> <</div>
            <div class="post-info">
              <h3><a href="<?=base_url().'article/'.$user['page_slug']?>"><?php echo ellipsize($user['title'],25,1);?></a></h3>
              <h6><?php echo $user['notice_date'];?></h6>
              <p><?php echo ellipsize($user['description'],200,1);?></p>

              <a class="readmore dark-color" href="#"><span>Read More</span></a> </div>

          </div>

        </div>
        <!--=== Post End ===-->
        <?php }?>
        <!--=== Post End ===-->

    </div>


      
    
    <?=$this->pagination->create_links();?>
            

  </section>

  <!--=== Blogs End ======-->

