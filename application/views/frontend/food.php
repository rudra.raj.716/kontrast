<section id="menu" class="parallax-section">
    <div class="about-title text-center">
        <h3><?php echo $location_name ?> Foods</h3>
    </div>
</section>

<div class="container">
    <ul class="nav nav-tabs">
        <li><a class="btn btn-secondary" onclick="getAllFoods()">All Foods</a></li>

        <?php
        foreach ($food_categories as $category) {

        ?>
            <li><a class="btn btn-secondary" onclick="getFood('<?php echo $category['id'] ?>','<?php echo $location_id ?>')"><?php echo $category['title'] ?></a></li>
        <?php
        }
        ?>

    </ul>

    <div class="container">
        <div class="row" id="food_content">

        </div>
    </div>


</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        getAllFoods();
    })

    function getAllFoods() {
        $('#food_content').empty();
        $.ajax({
            type: 'get',
            dataType: "json",
            url: "<?php echo base_url() ?>home/getAllFood/<?php echo $location_id ?>",
            success: function(data) {
                $("#food_content").append('<center><h4>All Foods</h4></center>');
                $.each(data, function(key, value) {
                    $("#food_content").append('<div class="col-md-6" style="margin-bottom:30px"><img style="height:320px;width:100%" src = " <?php echo base_url(); ?>assets/uploads/' + value.food_image + '" alt = ""class = "img-responsive" ><br><p>' + value.food_title + '<span class="pull-right">' + value.food_price + '</span></p><a class="btn btn-default" href="<?php echo base_url() ?>home/setCart/' + value.food_id + '">Add to Cart</a></div>');
                });
            }
        });
    }

    function getFood(id, location_id) {
        $('#food_content').empty();
        $.ajax({
            type: 'get',
            dataType: "json",
            url: "<?php echo base_url() ?>home/getFood/" + id + '/' + location_id,
            success: function(data) {
                console.log(data[1][0].title);
                $("#food_content").append('<center><h4>' + data[1][0].title + '</h4></center>');
                $.each(data[0], function(key, value) {
                    $("#food_content").append('<div class="col-md-6" style="margin-bottom:30px"><img style="height:320px;width:100%" src = " <?php echo base_url(); ?>assets/uploads/' + value.food_image + '" alt = ""class = "img-responsive" ><br><p>' + value.food_title + '<span class="pull-right">' + value.food_price + '</span></p><a class="btn btn-default" href="<?php echo base_url() ?>home/setCart/' + value.food_id + '">Add to Cart</a></div>');
                });
            }
        });

    }
</script>