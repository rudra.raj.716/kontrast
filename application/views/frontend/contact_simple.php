  <!--=== page-title-section start ===-->
  <section class="title-hero-bg contact-cover-bg" data-stellar-background-ratio="0.2">
    <div class="container">
      <div class="page-title text-center">
        <h1>Contact Simple</h1>
        <h4 class="text-uppercase mt-30 white-color">Customer Support 24 Hours</h4>
      </div>
    </div>
  </section>
  <!--=== page-title-section end ===-->

  <!--=== Contact Us Start ======-->
  <section class="contact-us dark-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <form name="contact-form" id="contact-form" action="http://www.incognitothemes.com/scoda/php/contact.php" method="POST">
              <?php if($error = $this->session->flashdata('msg')){ ?>
       <p style="color: green;"><strong>Success!</strong> <?php echo  $error; ?><p>
        <?php } ?>

            <div class="messages"></div>
            <div class="form-group">
              <label class="sr-only" for="name">Name</label>
              <input type="text" name="name" class="form-control" id="name" required="required" placeholder="Your Name" data-error="Your Name is Required">
              <div class="help-block with-errors mt-20"></div>
            </div>
            <div class="form-group">
              <label class="sr-only" for="email">Email</label>
              <input type="email" name="email" class="form-control" id="email" placeholder="Your Email" required="required" data-error="Please Enter Valid Email">
              <div class="help-block with-errors mt-20"></div>
            </div>
            <div class="form-group">
              <label class="sr-only" for="subject">Subject</label>
              <input type="text" name="subject" class="form-control" id="subject" placeholder="Your Subject">
            </div>
            <div class="form-group">
              <label class="sr-only" for="message">Message</label>
              <textarea name="message" class="form-control" id="message" rows="7" placeholder="Your Message" required data-error="Please, Leave us a message"></textarea>
              <div class="help-block with-errors mt-20"></div>
            </div>
            <button type="submit" name="submit" class="btn btn-color btn-circle">Send Mail</button>
          </form>
        </div>
        <div class="col-md-4">
          <h3 class="white-color">Postal Location</h3>
          <address>
          PO Box 16122 Toronto Eaton Centre,<br>
          220 The PATH Toronto, ON M5B 2H1, Canada<br>
          <abbr title="Phone">P:</abbr> (+1) 613 555-0105<br>
          <a href="mailto:#">alphadot@example.com</a>
          </address>
          <h3 class="white-color">Global Location</h3>
          <address>
          PO Box 16122 Toronto Eaton Centre,<br>
          220 The PATH Toronto, ON M5B 2H1, Canada<br>
          <abbr title="Phone">P:</abbr> (+1) 613 555-0105
          </address>
          <h3 class="white-color">Work Timings</h3>
          <p> <span>Mon - Fri: 10am to 7pm</span> <br>
            <span>Sat : 8am to 12pm</span> </p>
        </div>
      </div>
    </div>
  </section>
  <!--=== Contact Us End ======-->

  <!--=== Google Maps Start ======-->
  <section class="pt-0 pb-0 map-section">
    <div id="myMap" class="wide"></div>
  </section>
  <!--=== Google Maps End ======-->

  <!--=== Clients Start ======-->
  <section class="pt-50 pb-50 white-bg">
    <div class="container">
      <div class="row">
        <div id="client-slider" class="owl-carousel">
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/1.png" alt="01"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/2.png" alt="02"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/3.png" alt="03"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/4.png" alt="04"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/5.png" alt="05"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/6.png" alt="06"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/7.png" alt="07"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/8.png" alt="08"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/9.png" alt="09"/> </div>
          <div class="client-logo"> <img class="img-responsive" src="<?php echo base_url();?>assets/images/clients/10.png" alt="10"/> </div>
        </div>
      </div>
    </div>
  </section>
  <!--=== Clients End ======-->



  <!--=== GO TO TOP  ===-->
  <a href="#" id="back-to-top" title="Back to top">&uarr;</a>
  <!--=== GO TO TOP End ===-->

</div>
<!--=== Wrapper End ======-->

<!--=== Javascript Plugins ======-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1eO-IAC9PhPE5APzZDSs8W40ewuxnsu8"></script>
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>
<script src="<?php echo base_url();?>assets/js/master.js"></script>
<script src="<?php echo base_url();?>assets/js/bootsnav.js"></script>
<!--=== Javascript Plugins End ======-->

</body>

<!-- Mirrored from www.incognitothemes.com/scoda/contact-simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2021 07:05:11 GMT -->
</html>
