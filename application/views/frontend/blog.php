<?php //pr($notice); ?>
<!-- Page Title Section -->
    <div class="page-title-section">
    	<div class="auto-container">
			<ul class="post-meta">
				<li><a href="<?php echo base_url(); ?>">Index</a></li>
				<li>Blog</li>
			</ul>
			<h2>From the <span>Blog</span> </h2>
		</div>
	</div>
	<!-- End Page Title Section -->
	
	<!--Sidebar Page Container-->
    <div class="sidebar-page-container padding-top">
    	<div class="auto-container">
			<div class="row clearfix">
            	
                <!-- Content Side -->
                <div class="content-side col-lg-12 col-md-12 col-sm-12">
					<div class="our-blogs">
						
						<!-- News Block Three -->
						<?php foreach($notice as $n) { ?>
							<div class="news-block-three">
								<div class="inner-box">
									<div class="image">
										<a href="<?php echo base_url(); ?>blog-details/<?php echo $n->page_slug; ?>"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $n->notice_image; ?>" alt="" /></a>
									</div>
									<h4><a href="<?php echo base_url(); ?>blog-details/<?php echo $n->page_slug; ?>"><?php echo $n->title ?></a></h4>
									<p><?php echo $n->notice_intro; ?></p>
									<div class="post-date"><?php echo date('jS F Y',strtotime($n->notice_date)); ?></div>
								</div>
							</div>
						<?php } ?>
						
						
					</div>
					
					<!-- Post Share Options -->
					<div class="styled-pagination">
						<?php echo $links; ?>
					</div>
					
				</div>
				
				
				
			</div>
		</div>
	</div>

	<script>
$(function()
{
	$('.main-header').addClass('style-three');
	$('.main-footer').addClass('style-two');
});
</script>