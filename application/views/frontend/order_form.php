<?php
// print_r($totalAmt);
// die;
?>

<div class="container">
    <div class="row">
        <div class="card">

            <div class="card text-center">
                <div class="card-header">
                    <h2>Proceed to Checkout</h2>
                    <p>Please fill the form</p>

                </div>
            </div>

            <form action="<?php echo base_url('home/orders') ?>" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <h4><u>Shipping Information</u></h4>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="shipping_name" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Address</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="shipping_address" placeholder="Address">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" required class="form-control" name="shipping_email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Phone Number</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="shipping_phone" placeholder="Phone Number">
                            </div>
                        </div>

                    </div>

                    <?php
                    $ci = get_instance();
                    if ($ci->ion_auth->logged_in()) {
                    ?>
                        <div class="col-md-6">
                            <h4><u>Billing Information</u></h4>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" required class="form-control" value="<?php echo Filter::current_user()->first_name . ' ' . Filter::current_user()->last_name ?>" name="billing_name" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" required class="form-control" value="<?php echo Filter::current_user()->company ?>" name="billing_address" placeholder="Address">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" required class="form-control" value="<?php echo Filter::current_user()->email ?>" name="billing_email" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Phone Number</label>
                                <div class="col-sm-10">
                                    <input type="text" required class="form-control" value="<?php echo Filter::current_user()->phone ?>" name="billing_phone" placeholder="Phone Number">
                                </div>
                            </div>

                        </div>
                    <?php
                    } else {
                    ?>
                        <div class="col-md-6">
                            <h4><u>Billing Information</u></h4>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" required class="form-control" name="billing_name" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" required class="form-control" name="billing_address" placeholder="Address">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" required class="form-control" name="billing_email" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Phone Number</label>
                                <div class="col-sm-10">
                                    <input type="text" required class="form-control" name="billing_phone" placeholder="Phone Number">
                                </div>
                            </div>

                        </div>
                    <?php
                    }

                    ?>


                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        Total Amount: <?php echo $totalAmt ?>
                        <br>
                        Discount (%): <span id="dis">0</span>
                        <br>
                        Discount Amount: <span id="disAmt">0</span>
                        <br>
                        Use Discount Coupon: <input type="text" name="coupon_code" id="coupon_code">
                        <input class="btn btn-default btn-sm" id="btnDiscount" type="submit" name="discount" value="Use Code">
                        <br>
                        Amount to Pay: <span id="final_amt"><?php echo $totalAmt ?> </span>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $("#btnDiscount").click(function(e) {
        e.preventDefault();
        const code = $("#coupon_code").val();

        $.ajax({
            type: 'get',
            dataType: "json",
            url: "<?php echo base_url() ?>home/getDiscount/",
            success: function(data) {
                $.each(data, function(key, value) {

                    if (code == value.title) {
                        if (value.status == 'usable') {

                            $totAmt = '<?php echo $totalAmt ?>';
                            $disPer = value.discount;
                            $disAmt = $disPer / 100 * $totAmt;
                            $final = $totAmt - $disAmt;

                            $('#dis').html(value.discount);
                            $('#disAmt').html($disAmt);
                            $('#final_amt').html($final);

                        }
                    }

                });
            },
        });

    });
</script>