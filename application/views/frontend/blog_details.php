<!-- Page Title Section -->
    <div class="page-title-section style-two">
    	<div class="auto-container">
			<ul class="post-meta">
				<li><a href="<?php echo base_url(); ?>">Index</a></li>
				<li>Blog</li>
			</ul>
			<h2><?php echo $notice['title']; ?></h2>
		</div>
	</div>
	<!-- End Page Title Section -->
	
	<!--Sidebar Page Container-->
    <div class="sidebar-page-container padding-top style-two">
    	<div class="auto-container">
			<div class="row clearfix">
            	
                <!-- Content Side -->
                <div class="content-side col-lg-12 col-md-12 col-sm-12">
					<div class="blog-detail">
						<div class="inner-box">
							<!-- <div class="image">
								<img src="<?php //echo base_url(); ?>assets/uploads/<?php //echo $notice['notice_image']; ?>" alt="" />
							</div> -->
							<div class="lower-content" style = 'padding-top: 0px;'>
								<div class="post-info"> <?php echo date('jS F Y',strtotime($notice['notice_date'])); ?> </div>
								
								<?php echo $notice['description']; ?>

								
								
							</div>
						</div>
					</div>
					
					
					
				</div>
				
				
				
			</div>
		</div>
		
		
		
	</div>

<script>
$(function()
{
	$('.main-header').addClass('style-three');
	$('.main-footer').addClass('style-two');
});
</script>