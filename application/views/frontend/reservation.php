<section id="menu" class="parallax-section">
    <div class="container section-top-border">
        <section id="about" class="parallax-section">
            <div class="about-title text-center">
                <h3>Reservations</h3>
            </div>

        </section>
        <div class="section-title text-center">
            <h5>SELECT A LOCATION</h5>
        </div>
        <div class="row">
            <?php
            foreach ($locations as $location) {
            ?>
                <div class="col-md-6 col-sm-12 wow bounceIn animated" style="visibility: visible; animation-name: bounceIn;text-align: center;">
                    <div class="menu-item-wrapper">
                        <h4><?php echo $location['location_title'] ?></h4>
                        <p><?php echo $location['location_subtitle'] ?></p>
                        <p><?php echo $location['location_description'] ?></p>

                    </div>
                    <!-- Button to Open the Modal -->
                    <button type="button" onclick="sendLocation( '<?php echo $location['location_title'] ?>' )" id="getLocation" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Make a reservation
                    </button>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</section>


<div class="container">
    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Make a reservation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="<?php echo site_url('home/reservation_store') ?>" method="POST">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Location</label>
                            <div class="col-sm-10">
                                <input type="text" id="location" name="location" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" required class="form-control" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" required class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Phone Number</label>
                            <div class="col-sm-10">
                                <input type="text" name="phone" required class="form-control" placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Date</label>
                            <div class="col-sm-10">
                                <input type="date" name="date" required class="form-control" min="<?php echo date("Y-m-d"); ?>" placeholder="Select Date">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Time</label>
                            <div class="col-sm-10">
                                <input type="time" name="time" required class="form-control" placeholder="Select Time">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputPassword3" class="col-sm-2 col-form-label">Number of Guests</label>
                            <div class="col-sm-10">
                                <input type="number" required name="no_of_guests" class="form-control" placeholder="Number of guests">
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Submit Details</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script>
    function sendLocation(location) {
        $('#location').val(location);
    }
</script>