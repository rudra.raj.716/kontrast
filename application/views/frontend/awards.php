<div class="banner-text-left lernen_banner bg-services" style = "padding: 80px 0 80px;
    background-position: top center!important; background: linear-gradient(rgba(0, 0, 0, .6), rgba(0, 0, 0, .2)), url(<?php echo base_url();?>assets/img/avanibanner.jpg); background-size: cover!important; text-align: center; position: relative; ">
        <div class="container">
            <div class="row">
              <div class = 'col-md-12'>
                <div class="lernen_banner_title" style = "display: block; width: 100%;">
                    <h1 style = 'text-align:center; color:#fff; display:block; '>Awards</h1>
                </div>
              </div>
            </div>
        </div>
</div>



<div class="awards_area">
  <div class="container">
   
    <div class="row">
      <div class="col-md-12">
        <div class = "client_inner_area">

          <div class = "row">

          <?php foreach ($awards as $award) { ?>
            <div class = "col-md-3">
            <img style = 'cursor:pointer;' class = "neologo" src ="<?php echo base_url(); ?>assets/uploads/<?php echo $award['award_image']; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $award['award_image_description']; ?>" alt="<?php echo $award['award_image_alt']; ?>">
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
