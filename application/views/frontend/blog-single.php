<!-- blog section
================================================== -->
<section id="blog" class="parallax-section">
	<div class="container">
		<div class="row">

			<div class="col-md-8 col-sm-7">

				<div class="blog-image wow fadeInUp" data-wow-delay="0.9s">
					<img src="<?php echo base_url() ?>assets/uploads/<?php echo $notice['notice_image'] ?>" class="img-responsive" alt="<?php echo $notice['notice_image_alt'] ?>">
				</div>

				<div class="blog-content wow fadeInUp" data-wow-delay="1s">
					<span class="meta-date"><a href="#"><?php echo $notice['notice_date'] ?></a></span>
					<span class="meta-comments"><a href="#blog-comments">10 Comments</a></span>
					<span class="meta-author"><a href="#blog-author"><?php echo $notice['author'] ?></a></span>
					<h3><?php echo $notice['title'] ?></h3>
					<p><?php echo $notice['description'] ?></p>

				</div>

				<div class="blog-author wow fadeInUp" data-wow-delay="1s">
					<div class="media">
						<div class="media-object pull-left">
							<a href="#"><img src="<?php echo base_url() ?>assets/frontend/images/blog-author.png" class="img-responsive img-circle" alt="blog"></a>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Kontrst Author</h4>
							<p>Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet, wisi risus purus augue vulputate voluptate neque, curabitur.</p>
						</div>
					</div>
				</div>

				<div class="blog-comment wow fadeInUp" data-wow-delay="1s">
					<h3>2 Comments</h3>
					<div class="media">
						<div class="media-object pull-left">
							<img src="<?php echo base_url() ?>assets/frontend/images/blog-author.png" class="img-responsive img-circle" alt="blog">
						</div>
						<div class="media-body">
							<h4 class="media-heading">John Doe</h4>
							<h5>23 April 2021</h5>
							<p>Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet, wisi risus purus augue vulputate voluptate neque, curabitur.</p>
							<div class="media">
								<div class="media-object pull-left">
									<img src="<?php echo base_url() ?>assets/frontend/images/blog-author.png" class="img-responsive img-circle" alt="blog">
								</div>
								<div class="media-body">
									<h4 class="media-heading">Jennie Doe</h4>
									<h5>23 April 2015</h5>
									<p>Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet, wisi risus purus augue vulputate voluptate neque, curabitur.</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="blog-comment-form wow fadeInUp" data-wow-delay="1s">
					<h3>Leave Comment</h3>
					<form action="#" method="post">
						<div class="col-md-6 col-sm-6">
							<input type="text" class="form-control" placeholder="Name" name="name" required>
						</div>
						<div class="col-md-6 col-sm-6">
							<input type="email" class="form-control" placeholder="Email" name="email" required>
						</div>
						<div class="col-md-12 col-sm-12">
							<textarea class="form-control" placeholder="Message" rows="7" name="message" required></textarea>
						</div>
						<div class="col-md-3 col-sm-3">
							<input type="submit" class="form-control" value="Submit">
						</div>
					</form>
				</div>

			</div>

			<div class="col-md-4 col-sm-5 wow fadeInUp" data-wow-delay="1.3s">

				<div class="recent-post">
					<h3>Recent Posts</h3>

					<div class="media">
						<div class="media-object pull-left">
							<a href="#"><img src="<?php echo base_url() ?>assets/frontend/images/blog-image.jpg" class="img-responsive" alt="blog"></a>
						</div>
						<div class="media-body">
							<h5>22 April 2020</h5>
							<h4 class="media-heading"><a href="#">New Cuisine launches</a></h4>
						</div>
					</div>
					<div class="media">
						<div class="media-object pull-left">
							<a href="#"><img src="<?php echo base_url() ?>assets/frontend/images/blog-image.jpg" class="img-responsive" alt="blog"></a>
						</div>
						<div class="media-body">
							<h5>23 April 2020</h5>
							<h4 class="media-heading"><a href="#">Meet our talent chefs</a></h4>
						</div>
					</div>
					<div class="media">
						<div class="media-object pull-left">
							<a href="#"><img src="<?php echo base_url() ?>assets/frontend/images/blog-image.jpg" class="img-responsive" alt="blog"></a>
						</div>
						<div class="media-body">
							<h5>24 April 2020</h5>
							<h4 class="media-heading"><a href="#">Learn with us, Kontrast Training</a></h4>
						</div>
					</div>
				</div>

				<div class="blog-categories">
					<h3>Blog Categories</h3>
					<li><a href="#">Culinary and Discipline</a></li>
					<li><a href="#">Culinary Services</a></li>
					<li><a href="#">Business & Expansion</a></li>
					<li><a href="#">Events & Ceremonies</a></li>
				</div>

			</div>


		</div>
	</div>
</section>