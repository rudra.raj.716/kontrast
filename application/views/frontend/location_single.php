<section id="about" class="parallax-section">
    <div class="container">
        <div class="about-title text-center">
            <h4><?php echo $location['location_title'] ?></h4>
        </div>
        <div class="row">
            <div class="col-md-5 col-sm-10 wow fadeInUp" data-wow-delay="1.6s">
                <div class="img-border-wrapper">
                    <img class="img-responsive" src="<?php echo base_url(); ?>assets/uploads/<?php echo $location['location_image'] ?>" alt="">
                </div>
            </div>
            <div class="col-md-7 col-sm-10 text-justify wow fadeInUp" data-wow-delay="1.3s">
                <p><?php echo $location['location_subtitle'] ?></p>
                <p><?php echo $location['location_description'] ?></p>
            </div>

        </div>
    </div>
</section>


<!-- Menu
================================================== -->
<section id="menu" class="parallax-section">
    <div class="container section-top-border">
        <div class="section-title text-center">
            <h1>Our Menu</h1>
        </div>
        <div class="row">
            <?php foreach ($menu_categories as $key => $value) {
            ?>
                <div class="col-md-6 col-sm-12 wow bounceIn">
                    <div class="menu-item-wrapper" style="height:auto">
                        <h4><?php echo $key ?></h4>
                        <div class="menu-items">
                            <?php
                            foreach ($value as $v) {
                            ?>
                                <p><?php echo $v['food_title'] ?> <span class="pull-right"><?php echo $v['food_price'] ?></span></p>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>


        </div>
    </div>
</section>