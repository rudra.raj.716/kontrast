 <!-- Notice section
================================================== -->
 <section class="notice-section">
   <div class="notice-warpper container">
     <div class="row">
       <div class="col-md-6">
         <p class="notice-title mb-0">KONTRAST RESTAURANG NOW OPEN</p>
       </div>
       <div class="col-md-6">
         <p class="notice-text mb-0">Per use an electric mix of favourite</p>
       </div>
     </div>
   </div>
 </section>


 <!-- about section
================================================== -->
 <?php
  foreach ($about as $abt) {
  ?>
   <section id="about" class="parallax-section">
     <div class="container">
       <div class="about-title text-center">
         <h4><?php echo $abt['aboutus_title'] ?></h4>
       </div>
       <div class="row">
         <div class="col-md-5 col-sm-10 wow fadeInUp" data-wow-delay="1.6s">
           <div class="img-border-wrapper">
             <img class="img-responsive" src="<?php echo base_url(); ?>assets/uploads/<?php echo $abt['aboutus_image'] ?>" alt="<?php echo $abt['aboutus_image_alt'] ?>">
           </div>
         </div>
         <div class="col-md-7 col-sm-10 text-justify wow fadeInUp" data-wow-delay="1.3s">
           <p><?php echo $abt['aboutus_description'] ?></p>

         </div>

       </div>
     </div>
   </section>

 <?php
  }
  ?>


 <!-- services section
================================================== -->
 <section id="services" class="parallax-section">
   <div class="container section-top-border">
     <div class="row">
       <div class="col-md-4">
         <img src="<?php echo base_url(); ?>assets/frontend/images/s1.png" alt="" class="img-responsive">
       </div>
       <div class="col-md-4">
         <img src="<?php echo base_url(); ?>assets/frontend/images/s2.png" alt="" class="img-responsive">
       </div>
       <div class="col-md-4">
         <img src="<?php echo base_url(); ?>assets/frontend/images/s3.png" alt="" class="img-responsive">
       </div>
     </div>
   </div>
 </section>

 <!-- blog section
================================================== -->
 <section id="blog" class="parallax-section">
   <div class="container section-top-border">
     <div class="row">

       <!-- <div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 wow bounceIn text-center">
				<div class="section-title">
					<h1>Our Blog/Event</h1>
				</div>
			</div> -->
       <?php
        foreach ($notices as $notice) {
        ?>
         
         <div class="col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0.9s">
           <div class="blog-wrapper" >
             <style>
             .blog-wrapper:hover {
             background: url('<?php echo base_url() ?>assets/uploads/<?php echo $notice['notice_image']; ?>');
           }
         </style>
             <span class="blog-date"><?php echo $notice['notice_date'] ?></span>
             <h3 class="blog-title"><a href="<?php echo $notice['page_url'] ?>"><?php echo $notice['title'] ?></a></h3>
             <h5 id="blog-author">By <?php echo $notice['author'] ?> </h5>
           </div>
         </div>
       <?php
        }
        ?>



     </div>
   </div>
 </section>


 <!-- Menu
================================================== -->
 <section id="menu" class="parallax-section">
   <div class="container section-top-border">
     <div class="section-title text-center">
       <h1>Our Menu</h1>
     </div>
     <div class="row">
       <?php foreach ($menu_categories as $key => $value) {
        ?>
         <div class="col-md-6 col-sm-12 wow bounceIn">
           <div class="menu-item-wrapper" style="height:auto">
             <h4><?php echo $key ?></h4>
             <div class="menu-items">
               <?php
                foreach ($value as $v) {
                ?>
                 <p><?php echo $v['item_name'] ?> <span class="pull-right"><?php echo $v['item_price'] ?></span></p>
               <?php
                }
                ?>
             </div>
           </div>
         </div>
       <?php
        }
        ?>


     </div>
   </div>
 </section>


 <!-- Contact 
================================================== -->
 <section id="contact" class="parallax-section">
   <div class="container section-top-border">
     <div class="row">

       <div class="col-md-offset-2 col-md-8 col-sm-12">
         <div class="section-title">
           <h1>Get in touch</h1>
         </div>
       </div>

       <div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 wow fadeIn" data-wow-delay="0.6s">
         <form action="<?php echo base_url() . 'contact' ?>" method="post">
           <div class="col-md-6 col-sm-6">
             <input type="text" class="form-control" placeholder="Name" name="name" required>
             <input type="email" class="form-control" placeholder="Email" name="email" required>
             <input type="text" class="form-control" placeholder="Subject" name="subject" required>
             <input type="telephone" class="form-control" placeholder="Phone" name="phone" required>
           </div>
           <div class="col-md-6 col-sm-6">
             <textarea class="form-control" placeholder="Message" name="message" required></textarea>
           </div>
           <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
             <input type="submit" class="form-control" value="SEND MESSAGE">
           </div>
         </form>
       </div>

     </div>
   </div>
 </section>