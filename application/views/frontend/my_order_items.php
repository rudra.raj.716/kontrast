<div class="container">
    <div class="row">
        <div class="card">
            <div class="card text-center">
                <div class="card-header">
                    <h2>Items Orders</h2>
                    <p>List of my ordered items</p>

                </div>
            </div>

            <?php
            if (!empty($items)) {
            ?>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td><b>Food</b></td>
                                <td><b>Price</b></td>
                                <td><b>Quantity</b></td>
                                <td><b>Total</b></td>
                            </tr>
                        </thead>
                        <?php
                        $sum = 0;
                        foreach ($items as $item) {
                            $sum += $item['food_total_price'];
                        ?>
                            <tbody>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url() ?>assets/uploads/<?php echo $item['food_image'] ?>" height="auto" width="150" alt="">
                                        <?php echo $item['food_name'] ?>
                                    </td>
                                    <td><?php echo $item['food_price'] ?></td>
                                    <td><?php echo $item['food_quantity'] ?></td>
                                    <td><?php echo $item['food_total_price'] ?></td>
                                </tr>
                            </tbody>
                        <?php
                        }
                        ?>

                    </table>
                    <span class="pull-right" style="margin-right:7%">
                        <h5>Net Total: <?php echo $sum ?></h5>
                    </span>

                </div>
            <?php
            } else {
            ?>
                <div class="card text-center">
                    <div class="card-header">
                        <h3><i>No Items to Display</i></h3>
                    </div>
                </div>

            <?php
            }
            ?>

        </div>
    </div>
</div>