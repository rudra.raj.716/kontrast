<div class="card text-center">
	<div class="card-header">
		<h2><span><?php echo $page['page_title']; ?></span></h2>

		<div class="sidebar-page-container padding-top">
			<p><?php echo $page['page_content']; ?></p>
		</div>

	</div>
</div>