<div class="container">
    <div class="row">
        <div class="card">
            <div class="card text-center">
                <div class="card-header">
                    <h2>My Cart</h2>
                    <p>List of my carts</p>

                </div>
            </div>

            <?php
            if (!empty($items)) {
            ?>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td><b>S.N.</b></td>
                                <td><b>Food</b></td>
                                <td><b>Price</b></td>
                                <td><b>Quantity</b></td>
                                <td><b>Total</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sum = 0;
                            $i = 1;
                            foreach ($items as $item) {
                                $sum += $item['subtotal'];
                            ?>

                                <tr>
                                    <td><?php echo $i ?></td>
                                    <td>
                                        <img src="<?php echo base_url() ?>assets/uploads/<?php echo $item['image'] ?>" height="auto" width="150" alt="">
                                        <?php echo $item['name'] ?>
                                    </td>
                                    <td><?php echo $item['price'] ?></td>
                                    <td><a class="badge badge-light btn-primary" style="margin-right: 20px;" href="<?php echo base_url() ?>home/cartItemDecrease/<?php echo $item['rowid'] ?>/<?php echo $item['qty'] ?>">-</a><?php echo $item['qty'] ?><a class="badge badge-light btn-primary" style="margin-left: 20px;" href="<?php echo base_url() ?>home/cartItemIncrease/<?php echo $item['rowid'] ?>/<?php echo $item['qty'] ?>">+</a></td>
                                    <td><?php echo $item['subtotal'] ?></td>
                                    <td><a class="btn btn-danger btn-sm" href="<?php echo base_url() ?>home/cartItemRemove/<?php echo $item['rowid'] ?>">X</a></td>
                                </tr>

                            <?php
                            $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                    <span class="pull-right" style="margin-right:7%">
                        <h5>Net Total: <?php echo $sum ?></h5>
                        <h4><a class="btn btn-default" href="<?php echo base_url('home/orderForm') ?>">Proceed to Checkout</a></h4>
                    </span>

                </div>
            <?php
            } else {
            ?>
                <div class="card text-center">
                    <div class="card-header">
                        <h3><i>No Items to Display</i></h3>
                    </div>
                </div>

            <?php
            }
            ?>

        </div>
    </div>
</div>