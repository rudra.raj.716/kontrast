<div class="container">
    <div class="row">
        <div class="card">
            <div class="card text-center">
                <div class="card-header">
                    <h2>My Orders</h2>
                    <p>List of my orders</p>

                </div>
            </div>

            <?php
            if (!empty($orders)) {
            ?>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td><b>Shipping Name</b></td>
                                <td><b>Shipping Email</b></td>
                                <td><b>Shipping Address</b></td>
                                <td><b>Shipping Phone</b></td>
                                <td><b>Billing Name</b></td>
                                <td><b>Billing Email</b></td>
                                <td><b>Billing Address</b></td>
                                <td><b>Billing Phone</b></td>
                                <td><b>Order Date</b></td>
                            </tr>
                        </thead>
                        <?php
                        foreach ($orders as $order) {
                        ?>
                            <tbody>
                                <tr>
                                    <td><?php echo $order['shipping_name'] ?></td>
                                    <td><?php echo $order['shipping_email'] ?></td>
                                    <td><?php echo $order['shipping_address'] ?></td>
                                    <td><?php echo $order['shipping_phone'] ?></td>
                                    <td><?php echo $order['billing_name'] ?></td>
                                    <td><?php echo $order['billing_email'] ?></td>
                                    <td><?php echo $order['billing_address'] ?></td>
                                    <td><?php echo $order['billing_phone'] ?></td>
                                    <td><?php echo $order['order_date'] ?></td>
                                    <td><a class="btn btn-default btn-sm" href="<?php echo base_url() ?>home/singleOrderView/<?php echo $order['id'] ?>">View</a></td>
                                </tr>
                            </tbody>
                        <?php
                        }
                        ?>

                    </table>

                </div>
            <?php
            } else {
            ?>
                <div class="card text-center">
                    <div class="card-header">
                        <h3><i>No Items to Display</i></h3>
                    </div>
                </div>

            <?php
            }
            ?>

        </div>
    </div>
</div>