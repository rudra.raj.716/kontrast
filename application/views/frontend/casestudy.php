<div class="banner-text-left lernen_banner bg-services" style = "padding: 80px 0 80px;
    background-position: top center!important; background: linear-gradient(rgba(0, 0, 0, .6), rgba(0, 0, 0, .2)), url(<?php echo base_url();?>assets/img/avanibanner.jpg); background-size: cover!important; text-align: center; position: relative; ">
        <div class="container">
            <div class="row">
              <div class = 'col-md-12'>
                <div class="lernen_banner_title" style = "display: block; width: 100%;">
                    <h1 style = 'text-align:center; color:#fff; display:block; '>Case Study</h1>
                </div>
              </div>
            </div>
        </div>
</div>


<div class="case_study_area">
  <div class="container">
    <div class="row">
    
      <?php
      foreach ($casestudy as $case) {
      ?>
        <div class="col-md-4">
          <div class="whatwedoblock">
          <a href = "<?php echo base_url();?>case/<?php echo $case['page_slug']; ?>" style = 'color: #981b1e; '><img src="<?php echo base_url() ?>assets/uploads/<?php echo $case['cover_image']; ?>" /></a>
            <!-- <img src = "img/avanieye.png" style=""> -->
            <h4><a href = "<?php echo base_url();?>case/<?php echo $case['page_slug']; ?>"><?php echo $case['cover_image_title']; ?></a></h4>
            <p><?php echo $case['cover_image_description']; ?></p>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
