<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class About_Model extends MY_Model
{
    public $_table = "about_us";
    public $primary_key = "aboutus_id";

    public function get_aboutUs()
    {
        return $this->db->get('about_us')->result_array();
    }
}
