<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Location_Model extends MY_Model
{
    public $_table = "location";
    public $primary_key = "location_id";

    public function get_location()
    {
        return $this->db->get('location')->result_array();
    }
}
