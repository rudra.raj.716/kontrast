<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_Model extends MY_Model
{
	public $_table = "services";
	public $primary_key = "service_id";
}