<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_Category_Model extends MY_Model
{
    public $_table = "menu_category";
    public $primary_key = "id";

    public function get_menu_category()
    {
        return $this->db->get('menu_category')->result_array();
    }
}
