<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notice_Model extends MY_Model
{
	public $_table = "notice";
	public $primary_key = "notice_id";

	public function get_notice()
	{
		return $this->db->select('*')->from('notice')->where('notice_type','news')->order_by(' notice_date', 'DESC')->get()->result_array();
	}

}
