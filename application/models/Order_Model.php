<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_Model extends MY_Model
{
    public $_table = "orders";
    public $primary_key = "id";

    public function set_order()
    {
        $ci = get_instance();
        $code = $this->db->select('discount')->from('discount_coupon')->where('title', $this->input->post('coupon_code'))->get()->row();

        if ($ci->ion_auth->logged_in()) {
            $data = array(
                'shipping_name' => $this->input->post('shipping_name'),
                'shipping_email' => $this->input->post('shipping_email'),
                'shipping_address' => $this->input->post('shipping_address'),
                'shipping_phone' => $this->input->post('shipping_phone'),
                'billing_name' => $this->input->post('billing_name'),
                'billing_email' => $this->input->post('billing_email'),
                'billing_address' => $this->input->post('billing_address'),
                'billing_phone' => $this->input->post('billing_phone'),
                'user_id' => Filter::current_user()->user_id,
                'discount' => $code->discount ?? ''
            );
        } else {
            $data = array(
                'shipping_name' => $this->input->post('shipping_name'),
                'shipping_email' => $this->input->post('shipping_email'),
                'shipping_address' => $this->input->post('shipping_address'),
                'shipping_phone' => $this->input->post('shipping_phone'),
                'billing_name' => $this->input->post('billing_name'),
                'billing_email' => $this->input->post('billing_email'),
                'billing_address' => $this->input->post('billing_address'),
                'billing_phone' => $this->input->post('billing_phone'),
                'user_id' => '',
                'discount' => $code->discount ?? ''
            );
        }

        $this->db->insert('orders', $data);
        $insert_id = $this->db->insert_id();

        $data = array(
            'status' => 'unusable'
        );
        $this->db->where('title', $this->input->post('coupon_code'));
        $this->db->update('discount_coupon', $data);
        return $insert_id;
    }
}
