<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reservation_Model extends MY_Model
{
    public $_table = "reservations";
    public $primary_key = "reservation_id";

    public function set_reservation()
    {
        $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'location' => $this->input->post('location'),
                'date' => $this->input->post('date'),
                'time' => $this->input->post('time'),
                'no_of_guests' => $this->input->post('no_of_guests'),
            );
        return $this->db->insert('reservations', $data);
    }
}
