<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_Model extends MY_Model
{
	public $_table = "menu";
	public $primary_key = "menu_id";

	public function get_menus()
	{
		return $this->db->select('*')->from('menu')->order_by('menu_order','ASC')->get()->result_array();
	}
}
