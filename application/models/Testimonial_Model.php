<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonial_Model extends MY_Model
{
	public $_table = "testimonials";
	public $primary_key = "testimonial_id";
}