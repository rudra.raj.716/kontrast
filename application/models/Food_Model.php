<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Food_Model extends MY_Model
{
    public $_table = "food";
    public $primary_key = "food_id";

    public function get_food()
    {
        return $this->db->get('food')->result_array();
    }
}
