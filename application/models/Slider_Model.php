<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider_Model extends MY_Model
{
	public $_table = "slider";
	public $primary_key = "slider_id";

	public function get_slider()
	{
		return $this->db->get('slider')->result_array();
	}

}